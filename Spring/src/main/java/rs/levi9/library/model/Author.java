package rs.levi9.library.model;

import javax.persistence.*;
import java.util.List;

@Table
@Entity
public class Author extends BaseEntity {

    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Book> bookList;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }
}