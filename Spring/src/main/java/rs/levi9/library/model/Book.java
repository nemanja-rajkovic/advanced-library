package rs.levi9.library.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Book extends BaseEntity {

    @Column
    private String title;

    @Column
    private Long userId;

    @Column
    private String isbn13;

    @Column(length = 2000)
    private String description;

    @Column
    private String maturityRating;

    @Column(length = 550)
    private String coverImage;

    @JsonInclude()
    @Transient
    private String state;

    @JsonInclude()
    @Transient
    private Boolean bookIsPrivate;

    @Column
    @Transient
    private Integer userRating;

    @OneToOne(cascade = CascadeType.ALL)
    private UserRating globalRating;

    @Column
    private Integer pageCount;

    @Column
    private Date publishedDate;

    @Column
    private String language;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Genre> genreList;

    @Column
    private String publisher;

    @ManyToOne(cascade = CascadeType.ALL)
    private Author author;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Comment> comments;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public Integer getUserRating() {
        return userRating;
    }

    public void setUserRating(Integer userRating) {
        this.userRating = userRating;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<Genre> getGenreList() {
        return genreList;
    }

    public void setGenreList(List<Genre> genreList) {
        this.genreList = genreList;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public UserRating getGlobalRating() {
        return globalRating;
    }

    public void setGlobalRating(UserRating globalRating) {
        this.globalRating = globalRating;
    }

    public String getMaturityRating() {
        return maturityRating;
    }

    public void setMaturityRating(String maturityRating) {
        this.maturityRating = maturityRating;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean getBookIsPrivate() {
        return bookIsPrivate;
    }

    public void setBookIsPrivate(Boolean bookIsPrivate) {
        this.bookIsPrivate = bookIsPrivate;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}