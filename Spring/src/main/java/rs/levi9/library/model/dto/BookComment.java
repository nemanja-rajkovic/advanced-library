package rs.levi9.library.model.dto;

import java.util.Date;

public class BookComment {

    private Long commentId;

    private String text;

    private Date postTime;

    private String userName;

    private Long bookId;

    private String textToReply;

    private Long commentUserId;

    public BookComment() {
    }

    public BookComment(String text, Date postTime, String userName, Long bookId, Long commentUserId) {
        this.text = text;
        this.postTime = postTime;
        this.userName = userName;
        this.bookId = bookId;
        this.commentUserId = commentUserId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getCommentUserId() {
        return commentUserId;
    }

    public void setCommentUserId(Long commentUserId) {
        this.commentUserId = commentUserId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getTextToReply() {
        return textToReply;
    }

    public void setTextToReply(String textToReply) {
        this.textToReply = textToReply;
    }
}
