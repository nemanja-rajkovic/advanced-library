package rs.levi9.library.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Notification extends BaseEntity {

    @Column
    private String text;

    @Column
    private Long bookId;

    @Column
    private Long commentId;

    @Column private Boolean notificationIsSeen;

    public Notification() {
    }

    public Notification(String text, Long commentId, Boolean notificationIsSeen) {
        this.text = text;
        this.commentId = commentId;
        this.notificationIsSeen = notificationIsSeen;
    }

    public Notification(String text, Long bookId, Long commentId, Boolean notificationIsSeen) {
        this.text = text;
        this.bookId = bookId;
        this.commentId = commentId;
        this.notificationIsSeen = notificationIsSeen;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Boolean getNotificationIsSeen() {
        return notificationIsSeen;
    }

    public void setNotificationIsSeen(Boolean notificationIsSeen) {
        this.notificationIsSeen = notificationIsSeen;
    }
}
