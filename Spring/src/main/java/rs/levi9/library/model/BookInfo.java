package rs.levi9.library.model;

import javax.persistence.*;

@Entity
@Table
public class BookInfo extends BaseEntity {

    @OneToOne(cascade = CascadeType.ALL)
    private LibraryUser libraryUser;

    @OneToOne(cascade = CascadeType.ALL)
    private Book book;

    @Column
    private String state;

    private Boolean bookIsPrivate = false;

    @Column
    private Integer userRating;

    public BookInfo() {
    }

    public BookInfo(LibraryUser libraryUser, Book book) {
        this.libraryUser = libraryUser;
        this.book = book;
    }


    public BookInfo(LibraryUser libraryUser, Book book, String state, Boolean bookIsPrivate, Integer userRating) {
        this.libraryUser = libraryUser;
        this.book = book;
        this.state = state;
        this.bookIsPrivate = bookIsPrivate;
        this.userRating = userRating;
    }

    public BookInfo(LibraryUser libraryUser, Book book, String state, Boolean bookIsPrivate) {
        this.libraryUser = libraryUser;
        this.book = book;
        this.state = state;
        this.bookIsPrivate = bookIsPrivate;
    }

    public void setBookToRead() {
        this.setState("READ");
    }

    public void setBookToReading() {
        this.setState("READING");
    }

    public void setBookToToRead() {
        this.setState("TOREAD");
    }

    public LibraryUser getLibraryUser() {
        return libraryUser;
    }

    public void setLibraryUser(LibraryUser libraryUser) {
        this.libraryUser = libraryUser;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean getBookIsPrivate() {
        return bookIsPrivate;
    }

    public void setBookIsPrivate(Boolean bookIsPrivate) {
        this.bookIsPrivate = bookIsPrivate;
    }

    public Integer getUserRating() {
        return userRating;
    }

    public void setUserRating(Integer userRating) {
        this.userRating = userRating;
    }
}
