package rs.levi9.library.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class UserRating extends BaseEntity {

    private int numberOfVotes;
    private Double votes;

    public int getNumberOfVotes() {
        return numberOfVotes;
    }

    public void setNumberOfVotes(int numberOfVotes) {
        this.numberOfVotes = numberOfVotes;
    }

    public Double getVotes() {
        return votes;
    }

    public void setVotes(Double votes) {
        this.votes = votes;
    }
}