package rs.levi9.library.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Genre extends BaseEntity{

    private String genre;

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}