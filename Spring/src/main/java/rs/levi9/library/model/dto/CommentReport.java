package rs.levi9.library.model.dto;

public class CommentReport {

    private Long commentId;
    private Long commentOwnerId;
    private Long reporterId;
    private String reportDescription;
    private Long bookId;

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getCommentOwnerId() {
        return commentOwnerId;
    }

    public void setCommentOwnerId(Long commentOwnerId) {
        this.commentOwnerId = commentOwnerId;
    }

    public Long getReporterId() {
        return reporterId;
    }

    public void setReporterId(Long reporterId) {
        this.reporterId = reporterId;
    }

    public String getReportDescription() {
        return reportDescription;
    }

    public void setReportDescription(String reportDescription) {
        this.reportDescription = reportDescription;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }
}
