package rs.levi9.library.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * This class is used as a model and will represent USer in application.
 *
 * @Date 6.8.2018.
 */
@Entity
@Table
public class LibraryUser extends BaseEntity {

    @Column
    private String username;

    @Column
    private String name;

    @Column
    private Boolean gender;

    @Column(length = 500000000)
    private byte[] pictureUrl;

    @Column
    private String description;

    @Column
    private Date dateJoined;

    @Column private Boolean emailNotificationsAllowed = false;

    @Column
    private String email;

    @Column
    private String password;

    @Column
    private Date blockedUntil;

    @Column
    private Boolean blocked;

    @Column
    private String accountConfirmationCode;

    @Column
    private Boolean emailConfirmed;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Notification> notifications;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Book> books;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(joinColumns = @JoinColumn(name = "library_user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles = new ArrayList<>(Arrays.asList(new Role(Role.RoleType.ROLE_USER)));

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBlockedUntil() {
        return blockedUntil;
    }

    public void setBlockedUntil(Date blockedUntil) {
        this.blockedUntil = blockedUntil;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public String getAccountConfirmationCode() {
        return accountConfirmationCode;
    }

    public void setAccountConfirmationCode(String accountConfirmationCode) {
        this.accountConfirmationCode = accountConfirmationCode;
    }

    public Boolean getEmailConfirmed() {
        return emailConfirmed;
    }

    public void setEmailConfirmed(Boolean emailConfirmed) {
        this.emailConfirmed = emailConfirmed;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public byte[] getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(byte[] pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public Boolean getEmailNotificationsAllowed() {
        return emailNotificationsAllowed;
    }

    public void setEmailNotificationsAllowed(Boolean emailNotificationsAllowed) {
        this.emailNotificationsAllowed = emailNotificationsAllowed;
    }

    public Date getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }
}