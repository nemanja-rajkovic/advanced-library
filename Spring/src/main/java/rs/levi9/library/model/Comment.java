package rs.levi9.library.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table
public class Comment extends BaseEntity {

    private String text;

    private Date postTime;

    private String textToReply;

    private Long userId;

    private String userName;

    public Comment() {
    }

    public Comment(String text, Date postTime, Long userId, String userName) {
        this.text = text;
        this.postTime = postTime;
        this.userId = userId;
        this.userName = userName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getPostTime() {
        return postTime;
    }

    public void setPostTime(Date postTime) {
        this.postTime = postTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTextToReply() {
        return textToReply;
    }

    public void setTextToReply(String textToReply) {
        this.textToReply = textToReply;
    }
}