package rs.levi9.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import rs.levi9.library.model.Book;
import rs.levi9.library.model.BookInfo;
import rs.levi9.library.model.LibraryUser;
import rs.levi9.library.repository.BookRepository;
import rs.levi9.library.repository.BookStatusRepository;
import rs.levi9.library.repository.UserRepository;
import rs.levi9.library.service.utils.EmailService;
import rs.levi9.library.service.utils.UtilsService;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rs.levi9.library.service.utils.RandomStringGenerator;


@Service
@Transactional
public class UserService {

    private UserRepository userRepository;
    private EmailService emailService;
    private BookRepository bookRepository;
    private BookStatusRepository bookStatusRepository;
    private BookService bookService;
    private static final String READ = "READ";
    private static final String READING = "READING";
    private static final String TOREAD = "TOREAD";

    @Autowired
    public UserService(UserRepository userRepository, EmailService emailService, BookRepository bookRepository, BookStatusRepository bookStatusRepository, BookService bookService) {
        this.userRepository = userRepository;
        this.emailService = emailService;
        this.bookRepository = bookRepository;
        this.bookStatusRepository = bookStatusRepository;
        this.bookService = bookService;
    }

    public LibraryUser getOne(Long id) {
        return userRepository.findOne(id);
    }

    public List<LibraryUser> getAll() {
        List<LibraryUser> users = userRepository.findAll();
        for (LibraryUser user : users) {
            if (user.getUsername().equals("admin")) {
                users.remove(user);
                break;
            }
        }
        return users;
    }

    public LibraryUser changePassword(LibraryUser libraryUser) {
        String password = libraryUser.getPassword();
        libraryUser = getOne(libraryUser.getId());
        libraryUser.setPassword(password);
        return save(libraryUser);
    }

    public LibraryUser block(LibraryUser libraryUser) {
        if (libraryUser.getBlockedUntil() == null) {
            libraryUser.setBlocked(true);
        }
        return userRepository.save(libraryUser);
    }

    public void deleteOne(Long id) {
        LibraryUser libraryUser = userRepository.findOne(id);

        if(libraryUser != null) {
            libraryUser.setNotifications(null);
            libraryUser.setRoles(null);
            for(int i = 0; i < libraryUser.getBooks().size(); i++){

                bookService.deleteBook(libraryUser.getBooks().get(i).getId(), id);
                i--;
            }
        }
        userRepository.save(libraryUser);
        libraryUser = userRepository.findOne(id);
        userRepository.delete(libraryUser);
    }

    public LibraryUser save(LibraryUser user) {
        return userRepository.save(user);
    }

    public LibraryUser update(LibraryUser user) {
        return userRepository.save(user);
    }

    public boolean checkIfUserExists(LibraryUser libraryUser) {
        return userRepository.findLibraryUserByEmail(libraryUser.getEmail()) != null || userRepository.findLibraryUserByUsername(libraryUser.getUsername()) != null;
    }

    public void unblockUserIfNeeded(LibraryUser user) {
        if (user.getBlocked() && user.getBlockedUntil() != null && user.getBlockedUntil().compareTo(new Date()) == -1) {
            user.setBlocked(false);
        }
    }

    public LibraryUser getUserByUsername(String username) {
        LibraryUser user = userRepository.findLibraryUserByUsername(username);
        unblockUserIfNeeded(user);
        return user;
    }

    public LibraryUser registerUser(LibraryUser libraryUser) throws MessagingException {
        emailService.sendEmail(new UtilsService().createEmailConfirmationMessage(libraryUser));
        libraryUser.setAccountConfirmationCode(new UtilsService().encodeToBase64(libraryUser.getUsername() + ":" + libraryUser.getEmail()));
        libraryUser.setBlocked(false);
        libraryUser.setEmailNotificationsAllowed(true);
        return save(libraryUser);
    }

    public boolean confirmCode(String code) {
        LibraryUser user = userRepository.findLibraryUserByAccountConfirmationCode(code);
        if (user != null) {
            setUserToRegistered(user);
            return true;
        }
        return false;
    }

    public void setUserToRegistered(LibraryUser libraryUser) {
        libraryUser.setEmailConfirmed(true);
        userRepository.save(libraryUser);
    }

    public LibraryUser findUserByEmail(String mail) {
        return userRepository.findLibraryUserByEmail(mail);
    }

    public boolean resetPassword(String email) throws MessagingException {
        LibraryUser user = findUserByEmail(email);
        if (user != null) {
            String[] parts = {new RandomStringGenerator().nextString().substring(0, 10), null};
            user.setPassword(parts[0]);
            userRepository.save(user);
            return emailService.sendEmail(new UtilsService().createResetPasswordMessage(user));
        }
        return false;
    }

    public List<Book> getReadBooks(Long id) {
        return convertBooks(bookStatusRepository.findBookStatusesByLibraryUserIdAndState(id, READ));
    }

    public List<Book> getReadingBooks(Long id) {
        return convertBooks(bookStatusRepository.findBookStatusesByLibraryUserIdAndState(id, READING));
    }

    public List<Book> getToReadBooks(Long id) {
        return convertBooks(bookStatusRepository.findBookStatusesByLibraryUserIdAndState(id, TOREAD));
    }

    public List<Book> convertBooks(List<BookInfo> statuses) {
        ArrayList<Book> books = new ArrayList<>();
        for (BookInfo bookInfo : statuses) {
            bookInfo.getBook().setState(bookInfo.getState());
            bookInfo.getBook().setBookIsPrivate(bookInfo.getBookIsPrivate());
            bookInfo.getBook().setUserRating(bookInfo.getUserRating());
            books.add(bookInfo.getBook());
        }
        return books;
    }

    public Boolean saveProfilePicture(Long id, MultipartFile profileImage) throws IOException {
        LibraryUser user = userRepository.findOne(id);
        user.setPictureUrl(profileImage.getBytes());
        userRepository.save(user);
        return true;
    }

    public List<LibraryUser> findUsersByQuery(String s) {
        List<LibraryUser> users = userRepository.findAllByNameOrUsernameContainingIgnoreCase(s, s);
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getUsername().equals("admin")) {
                users.remove(i);
                break;
            }
        }
        return users;
    }


    public Book updateBookStatus(Book book) {
        BookInfo bookInfo = bookStatusRepository.findBookStatusByLibraryUserIdAndBookId(book.getUserId(), book.getId());
        bookInfo.setUserRating(book.getUserRating());
        bookInfo.setBookIsPrivate(book.getBookIsPrivate());
        return bookRepository.save(book);
    }

    public void unblockUser(Long id) {
        LibraryUser user = userRepository.findOne(id);
        user.setBlocked(false);
        userRepository.save(user);
    }

    public List<Book> getAllUserBooks(Long userId) {
        List<Book> books = bookRepository.findAll();
        if (books != null) {
            for (int i = 0; i < books.size(); i++) {
                if (!books.get(i).getUserId().equals(userId)) {
                    books.remove(i);
                } else {
                    BookInfo bookInfo = bookStatusRepository.findBookStatusByLibraryUserIdAndBookId(userId, books.get(i).getId());
                    if (bookInfo != null) {
                        books.get(i).setUserRating(bookInfo.getUserRating());
                        books.get(i).setBookIsPrivate(bookInfo.getBookIsPrivate());
                    }
                }
            }
        }
        return books;
    }
}