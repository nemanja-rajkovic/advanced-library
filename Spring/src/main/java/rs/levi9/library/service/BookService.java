package rs.levi9.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.levi9.library.model.Book;
import rs.levi9.library.model.BookInfo;
import rs.levi9.library.model.Comment;
import rs.levi9.library.model.LibraryUser;
import rs.levi9.library.model.dto.BookComment;
import rs.levi9.library.repository.BookRepository;
import rs.levi9.library.repository.BookStatusRepository;
import rs.levi9.library.repository.CommentRepository;
import rs.levi9.library.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class BookService {

    private BookRepository bookRepository;
    private UserRepository userRepository;
    private BookStatusRepository bookStatusRepository;
    private CommentRepository commentRepository;

    @Autowired
    public BookService(BookRepository bookRepository, UserRepository userRepository, BookStatusRepository bookStatusRepository, CommentRepository commentRepository) {
        this.bookRepository = bookRepository;
        this.bookStatusRepository = bookStatusRepository;
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
    }

    public Book getOne(Long id) {
        return bookRepository.findOne(id);
    }

    public Book getOneByBookIdAndUserId(Long id, Long userId) {
        BookInfo bookInfo = bookStatusRepository.findBookStatusByLibraryUserIdAndBookId(userId, id);
        bookInfo.getBook().setBookIsPrivate((bookInfo.getBookIsPrivate() == null) ? false : bookInfo.getBookIsPrivate() == null);
        bookInfo.getBook().setState(bookInfo.getState());
        bookInfo.getBook().setUserId(userId);
        bookInfo.getBook().setUserRating(bookInfo.getUserRating());
        bookInfo.getBook().setBookIsPrivate(bookInfo.getBookIsPrivate());
        return bookInfo.getBook();
    }

    public void deleteBook(Long bookId, Long userId) {
        LibraryUser libraryUser = userRepository.findOne(userId);
        for (Book book : libraryUser.getBooks()) {
            if (book.getId().equals(bookId)) {
                libraryUser.getBooks().remove(book);
                break;
            }
        }
        userRepository.save(libraryUser);
        BookInfo bookInfo = bookStatusRepository.findBookStatusByLibraryUserIdAndBookId(userId, bookId);
        bookInfo.setLibraryUser(null);
        bookInfo.setBook(null);
        bookStatusRepository.save(bookInfo);
        bookStatusRepository.delete(bookInfo.getId());
    }

    public Comment saveComment(BookComment bookComment) {
        Book book = bookRepository.getOne(bookComment.getBookId());
        Comment comment = new Comment(bookComment.getText(), new Date(), bookComment.getCommentUserId(), bookComment.getUserName());
        if (bookComment.getTextToReply() != null) {
            comment.setTextToReply(bookComment.getTextToReply());
        }

        book.getComments().add(comment);
        bookRepository.save(book);
        book = bookRepository.getOne(bookComment.getBookId());
        return book.getComments().get(book.getComments().size() - 1);
    }

    public Book editComment(BookComment bookComment) {
        Book book = bookRepository.getOne(bookComment.getBookId());

        for (Comment comm : book.getComments()) {
            if (comm.getId().equals(bookComment.getCommentId())) {
                comm.setText(bookComment.getText());
                comm.setPostTime(new Date());
                break;
            }
        }
        return bookRepository.save(book);
    }

    public void deleteComment(Long id, Long bookId) {
        Book book = bookRepository.getOne(bookId);
        for (Comment comm : book.getComments()) {
            if (comm.getId().equals(id)) {
                book.getComments().remove(comm);
                commentRepository.delete(id);
                break;
            }
        }
        bookRepository.save(book);
    }

    public List<Comment> getComments(Long id) {
        return bookRepository.findOne(id).getComments();
    }

    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    public List<Book> search(String query) {
        return bookRepository.findAllByAuthorNameContainingIgnoreCaseOrTitleContainingIgnoreCase(query, query);
    }

    public void delete(Long id) {
        bookRepository.delete(id);
    }

    public LibraryUser save(Book book) throws IllegalArgumentException {
        Book book1 = bookRepository.findBookByIsbn13(book.getIsbn13());
        LibraryUser user = userRepository.getOne(book.getUserId());
        if (Objects.isNull(book1)) {
            book.setId(null);
            user.getBooks().add(book);
            bookStatusRepository.save(new BookInfo(user, book, book.getState(), book.getBookIsPrivate(), null));
            return userRepository.save(user);
        } else {
            if (bookStatusRepository.findBookStatusByLibraryUserIdAndBookId(book.getUserId(), book1.getId()) != null) {
                return null;
            }
            user.getBooks().add(book1);
            bookStatusRepository.save(new BookInfo(user, book1, book.getState(), book.getBookIsPrivate(), null));
            return userRepository.save(user);
        }
    }

    public Book update(Book book) {
        return bookRepository.save(book);
    }

    public void moveBookToReadList(Book book) {
        BookInfo bookInfo = bookStatusRepository.findBookStatusByLibraryUserIdAndBookId(book.getUserId(), book.getId());
        bookInfo.setBookToRead();
    }

    public void moveBookToToReadList(Book book) {
        BookInfo bookInfo = bookStatusRepository.findBookStatusByLibraryUserIdAndBookId(book.getUserId(), book.getId());
        bookInfo.setBookToToRead();
    }

    public void moveBookToReadingList(Book book) {
        BookInfo bookInfo = bookStatusRepository.findBookStatusByLibraryUserIdAndBookId(book.getUserId(), book.getId());
        bookInfo.setBookToReading();
    }

    public Comment getCommentById(Long commentId) {
        return commentRepository.findOne(commentId);
    }

}
