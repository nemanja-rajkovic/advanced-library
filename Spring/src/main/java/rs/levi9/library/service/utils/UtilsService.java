package rs.levi9.library.service.utils;


import rs.levi9.library.model.LibraryUser;
import rs.levi9.library.model.dto.EmailMessage;

import java.util.Base64;

public class UtilsService {

    public String encodeToBase64(String string) {
        return Base64.getEncoder().encodeToString(string.getBytes());
    }

    public EmailMessage createEmailConfirmationMessage(LibraryUser libraryUser) {
        // instead of localhost, we must switch it for ngrok url.
        String linkToConfirm = "Please click here in order to register your account! \n http://localhost:8080/confirm/" + encodeToBase64(libraryUser.getUsername() + ":" + libraryUser.getEmail());
        String subject = "Please confirm your email in order to use Library Application!";
        return new EmailMessage(libraryUser.getEmail(), subject, linkToConfirm);
    }

    public EmailMessage createResetPasswordMessage(LibraryUser libraryUser) {
        String text = "We received a 'reset password' request. \nThis is your new password: " + libraryUser.getPassword();
        String subject = "Password reset request";
        return new EmailMessage(libraryUser.getEmail(), subject, text);
    }
}