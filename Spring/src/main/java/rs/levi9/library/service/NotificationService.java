package rs.levi9.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.levi9.library.model.BookInfo;
import rs.levi9.library.model.LibraryUser;
import rs.levi9.library.model.Notification;
import rs.levi9.library.model.dto.BookComment;
import rs.levi9.library.model.dto.CommentReply;
import rs.levi9.library.model.dto.CommentReport;
import rs.levi9.library.model.dto.EmailMessage;
import rs.levi9.library.repository.BookStatusRepository;
import rs.levi9.library.repository.UserRepository;
import rs.levi9.library.service.utils.EmailService;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class NotificationService {

    private UserRepository userRepository;
    private BookStatusRepository bookStatusRepository;
    private EmailService emailService;

    @Autowired
    public NotificationService(UserRepository userRepository, BookStatusRepository bookStatusRepository, EmailService emailService) {
        this.userRepository = userRepository;
        this.bookStatusRepository = bookStatusRepository;
        this.emailService = emailService;
    }

    public List<Notification> getNotificationsById(Long id) {
        return userRepository.findOne(id).getNotifications();
    }

    public void notifyAdmin(CommentReport commentReport) {
        LibraryUser libraryUser = userRepository.findLibraryUserByUsername("admin");
        LibraryUser reporter = userRepository.getOne(commentReport.getReporterId());
        LibraryUser reported = userRepository.getOne(commentReport.getCommentOwnerId());
        String text = "User " + reporter.getUsername() + " has reported a comment posted by " + reported.getUsername() + ".";
        if (commentReport.getReportDescription() != null) {
            text = text + "\nReason: " + commentReport.getReportDescription();
        }
        Notification notification = new Notification(text, commentReport.getBookId(),commentReport.getCommentId(), false);
        libraryUser.getNotifications().add(notification);
    }

    public void notifyUsers(BookComment bookComment) throws MessagingException {
        List<BookInfo> statues = bookStatusRepository.findBookStatusesByBookId(bookComment.getBookId());
        String text = "";
        if(bookComment.getUserName() != null){
            text = bookComment.getUserName() + " commented on the " + bookStatusRepository.findOne(bookComment.getBookId()).getBook().getTitle();
        }else {
            text = "User commented on the " + bookStatusRepository.findOne(bookComment.getBookId()).getBook().getTitle();
        }

        for (BookInfo bookInfo : statues) {
            if (bookInfo.getLibraryUser().getId().equals(bookComment.getCommentUserId())) {
                continue;
            }
            Notification notification = new Notification(text, bookComment.getBookId(), bookComment.getCommentId(), false);
            bookInfo.getLibraryUser().getNotifications().add(notification);

            if (bookInfo.getLibraryUser().getEmailNotificationsAllowed() != null && bookInfo.getLibraryUser().getEmailNotificationsAllowed().equals(true)) {
                text = text + "\nPlease come back to our application in order to check your notifications!\n " +
                        "http://localhost:4200/#/login/" + bookComment.getBookId()+ "/" + bookComment.getCommentId();
                emailService.sendEmail(new EmailMessage(bookInfo.getLibraryUser().getEmail(), "Comment notification", text));
            }
        }
        bookStatusRepository.save(statues);
    }

    public void notifyUser(CommentReply commentReply) throws MessagingException {

        LibraryUser libraryUser = userRepository.findOne(commentReply.getCommentOwnerId());
        String text = commentReply.getUserName() + " replied to your comment";
        Notification notification = new Notification(text, commentReply.getBookId(), commentReply.getCommentId(), false);
        libraryUser.getNotifications().add(notification);
        if (libraryUser.getEmailNotificationsAllowed() != null && libraryUser.getEmailNotificationsAllowed().equals(true)) {
            text = text + "\n Please come back to our application in order to check your notifications!\n " +
                    "http://localhost:4200/#/login/" + commentReply.getBookId() + "/" + commentReply.getCommentId();
            emailService.sendEmail(new EmailMessage(libraryUser.getEmail(), "Comment reply notification", text));
        }
        userRepository.save(libraryUser);
    }

    public void deleteNotification(Long userId, Long id) {
        LibraryUser libraryUser = userRepository.findOne(userId);
        for (Notification notification : libraryUser.getNotifications()) {
            if (notification.getId().equals(id)) {
                libraryUser.getNotifications().remove(notification);
                break;
            }
        }
        userRepository.save(libraryUser);
    }

    public void updateNotification(Long userId, Long id) {
        LibraryUser libraryUser = userRepository.findOne(userId);
        for (Notification notification : libraryUser.getNotifications()) {
            if (notification.getId().equals(id)) {
                notification.setNotificationIsSeen(true);
                break;
            }
        }
        userRepository.save(libraryUser);
    }
}