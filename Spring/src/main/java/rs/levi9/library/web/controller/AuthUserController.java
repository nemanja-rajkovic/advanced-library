package rs.levi9.library.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.levi9.library.model.LibraryUser;
import rs.levi9.library.model.dto.AuthenticatedUser;
import rs.levi9.library.service.UserService;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/auth")
public class AuthUserController {

    private UserService userService;

    @Autowired
    public AuthUserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/user")
    public AuthenticatedUser getUser(Authentication authentication) {
        if(authentication == null){
            return null;
        }
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority authority : authentication.getAuthorities()) {
            roles.add(authority.getAuthority());
        }
        LibraryUser user = userService.getUserByUsername(authentication.getName());
        return new AuthenticatedUser(user.getId(),user.getName(), authentication.getName(), user.getEmail(), roles, user.getEmailConfirmed(), user.getBlocked());
    }
}
