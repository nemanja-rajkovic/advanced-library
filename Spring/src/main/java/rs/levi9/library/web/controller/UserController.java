package rs.levi9.library.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rs.levi9.library.model.Book;
import rs.levi9.library.model.LibraryUser;
import rs.levi9.library.model.dto.CommentReport;
import rs.levi9.library.service.NotificationService;
import rs.levi9.library.service.UserService;

import javax.mail.MessagingException;
import javax.print.attribute.standard.Media;
import javax.servlet.annotation.MultipartConfig;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/user")
@CrossOrigin
@MultipartConfig(maxFileSize = 1024 * 1024 * 1024, maxRequestSize = 1024 * 1024 * 1024)
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getOne(@PathVariable("id") Long id) {
        return new ResponseEntity(userService.getOne(id), HttpStatus.OK);
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getAll() {
        return new ResponseEntity(userService.getAll(), HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity post(@RequestBody LibraryUser libraryUser) {
        return new ResponseEntity(userService.save(libraryUser), HttpStatus.OK);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity put(@RequestBody LibraryUser libraryUser) {
        return new ResponseEntity(userService.update(libraryUser), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        userService.deleteOne(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/register")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity register(@RequestBody LibraryUser libraryUser) throws IllegalArgumentException, MessagingException {
        if (!userService.checkIfUserExists(libraryUser)) {
            return new ResponseEntity(userService.registerUser(libraryUser), HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/unblock/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity unblockUser(@PathVariable("id") Long id) {
        userService.unblockUser(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/block")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity block(@RequestBody LibraryUser libraryUser) {
        return new ResponseEntity(userService.block(libraryUser), HttpStatus.OK);
    }

    @PostMapping("/forgot-password")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity resetPassword(@RequestBody LibraryUser user) throws MessagingException {
        if (userService.resetPassword(user.getEmail())) {
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/read-books/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity findReadBooks(@PathVariable("id") Long id) {
        if (userService.getOne(id) != null) {
            return new ResponseEntity(userService.getReadBooks(id), HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/to-read-books/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity findToReadBooks(@PathVariable("id") Long id) {
        if (userService.getOne(id) != null) {
            return new ResponseEntity(userService.getToReadBooks(id), HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/reading-books/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity findReadingBooks(@PathVariable("id") Long id) {
        if (userService.getOne(id) != null) {
            return new ResponseEntity(userService.getReadingBooks(id), HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/post-read")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity postReadBook(@RequestBody Book book) {
        LibraryUser libraryUser = userService.getOne(book.getUserId());
        if (libraryUser != null) {
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/image/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity uploadImage(@RequestPart("profileImage") MultipartFile profileImage, @PathVariable("id") Long id) throws IOException {
        return new ResponseEntity(userService.saveProfilePicture(id, profileImage), HttpStatus.OK);
    }

    @GetMapping("/get-all")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getUsers() {
        return new ResponseEntity(userService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/get-all/{query}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getUsersByQuery(@PathVariable("query") String query) {
        return new ResponseEntity(userService.findUsersByQuery(query), HttpStatus.OK);
    }

    @PutMapping("/update-status")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity updateBookStatus(@RequestBody Book book) {
        return new ResponseEntity(userService.updateBookStatus(book), HttpStatus.OK);
    }

    @GetMapping("/get-all-books/{userId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getAllUserBooks(@PathVariable("userId") Long userId) {

            return new ResponseEntity(userService.getAllUserBooks(userId), HttpStatus.OK);
    }

    @PutMapping("/change-password")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity changePassword(@RequestBody LibraryUser user) {
        return new ResponseEntity(userService.changePassword(user), HttpStatus.OK);
    }
}