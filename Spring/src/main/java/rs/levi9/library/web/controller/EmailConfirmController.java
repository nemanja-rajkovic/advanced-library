package rs.levi9.library.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.levi9.library.service.UserService;

@RestController
@RequestMapping
public class EmailConfirmController {

    private UserService userService;

    @Autowired
    public EmailConfirmController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/confirm/{id}")
    public String confirmEmail(@PathVariable("id") String id) {
        if (userService.confirmCode(id)) {
            //here we should return some nice html page.
            return "<h1 style='text-align : center; top: 50%; color: gray;' >You have successfully created your account!</h1> <script> setTimeout(function(){ window.location.replace('http://localhost:4200'); }, 2000);</script>";

        } else {
            return "Something is wrong!";
        }
    }
}