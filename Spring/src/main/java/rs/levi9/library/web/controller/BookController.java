package rs.levi9.library.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.library.model.Book;
import rs.levi9.library.model.dto.BookComment;
import rs.levi9.library.service.BookService;

@RestController
@RequestMapping("/book")
@CrossOrigin
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/{id}/{userId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getOneWithUserId(@PathVariable("id") Long id, @PathVariable("userId") Long userId) {
        return new ResponseEntity(bookService.getOneByBookIdAndUserId(id, userId), HttpStatus.OK);
    }

    @GetMapping("/social-share/{id}")
    public ResponseEntity getOneForSocialSharing(@PathVariable("id") Long id) {
        return new ResponseEntity(bookService.getOne(id), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getOne(@PathVariable("id") Long id) {
        return new ResponseEntity(bookService.getOne(id), HttpStatus.OK);
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getAll() {
        return new ResponseEntity(bookService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/get-from-db/{query}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getByAuthor(@PathVariable("query") String query) {
        return new ResponseEntity(bookService.search(query), HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity post(@RequestBody Book book) {
        return new ResponseEntity(bookService.save(book), HttpStatus.OK);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity put(@RequestBody Book book) {
        return new ResponseEntity(bookService.update(book), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        bookService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/move-to-to-read-list")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity moveBookToToReadList(@RequestBody Book book) {
        bookService.moveBookToToReadList(book);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/move-to-read-list")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity moveBookToReadList(@RequestBody Book book) {
        bookService.moveBookToReadList(book);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/move-to-reading-list")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity moveBookToReadingList(@RequestBody Book book) {
        bookService.moveBookToReadingList(book);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/save-comment")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity saveComment(@RequestBody BookComment bookComment) {
        return new ResponseEntity(bookService.saveComment(bookComment), HttpStatus.OK);
    }

    @GetMapping("/get-comment/{bookId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getComments(@PathVariable("bookId") Long bookId) {
        return new ResponseEntity(bookService.getComments(bookId), HttpStatus.OK);
    }

    @PutMapping("/edit-comment")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity editComment(@RequestBody BookComment bookComment) {
        return new ResponseEntity(bookService.editComment(bookComment), HttpStatus.OK);
    }

    @DeleteMapping("/delete-comment/{id}/{bookId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity deleteComment(@PathVariable("id") Long id, @PathVariable("bookId") Long bookId) {
        bookService.deleteComment(id, bookId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("comment/{commentId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getCommentById(@PathVariable("commentId") Long commentId) {
        return new ResponseEntity(bookService.getCommentById(commentId), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{bookId}/{userId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getCommentById(@PathVariable("bookId") Long bookId, @PathVariable("userId") Long userId) {
        bookService.deleteBook(bookId, userId);
        return new ResponseEntity(HttpStatus.OK);
    }
}