package rs.levi9.library.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rs.levi9.library.model.dto.BookComment;
import rs.levi9.library.model.dto.CommentReply;
import rs.levi9.library.model.dto.CommentReport;
import rs.levi9.library.service.NotificationService;

import javax.mail.MessagingException;

@RestController
@RequestMapping("/notification")
@CrossOrigin
public class NotificationController {

    private NotificationService notificationService;


    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @PostMapping("/report-comment")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity reportComment(@RequestBody CommentReport commentReport) {
        notificationService.notifyAdmin(commentReport);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/notify-users")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity notifyUsers(@RequestBody BookComment bookComment) throws MessagingException {
        notificationService.notifyUsers(bookComment);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/notify-user")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity notifyUser(@RequestBody CommentReply commentReply) throws MessagingException {
        notificationService.notifyUser(commentReply);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity getNotificationsById(@PathVariable("id") Long id){
        return new ResponseEntity(notificationService.getNotificationsById(id),HttpStatus.OK);
    }

    @DeleteMapping("/{userId}/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity deleteNotification(@PathVariable("userId") Long userId,@PathVariable("id") Long id){
        notificationService.deleteNotification(userId,id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/{userId}/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity updateNotification(@PathVariable("userId") Long userId,@PathVariable("id") Long id){
        notificationService.updateNotification(userId,id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
