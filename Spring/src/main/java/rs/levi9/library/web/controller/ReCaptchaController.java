package rs.levi9.library.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.levi9.library.service.CaptchaService;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class ReCaptchaController {


    @Autowired
    public ReCaptchaController() {
    }

    @GetMapping("/captcha/{id}")
    public ResponseEntity receiveCaptcha(@PathVariable("id") String id){
        CaptchaService captchaService = new CaptchaService();
       if(captchaService.post(id,null ).contains("true")){
           return new ResponseEntity(true, HttpStatus.OK);
       }else {
           return new ResponseEntity(false, HttpStatus.FORBIDDEN);
       }
    }
}
