package rs.levi9.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.levi9.library.model.BookInfo;

import java.util.List;

@Repository
public interface BookStatusRepository extends JpaRepository<BookInfo, Long> {

        BookInfo findBookStatusByLibraryUserIdAndBookId(Long userId, Long bookId);
        List<BookInfo> findBookStatusesByLibraryUserIdAndState(Long id, String state);
        List<BookInfo> findBookStatusesByBookId(Long id);
}
