package rs.levi9.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.levi9.library.model.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {
}