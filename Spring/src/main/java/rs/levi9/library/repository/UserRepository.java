package rs.levi9.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.levi9.library.model.LibraryUser;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<LibraryUser, Long> {

    LibraryUser findLibraryUserByUsername(String username);
    LibraryUser findLibraryUserByEmail(String email);
    LibraryUser findLibraryUserByAccountConfirmationCode(String code);
    List<LibraryUser> findAllByNameOrUsernameContainingIgnoreCase(String s1, String s2);

}