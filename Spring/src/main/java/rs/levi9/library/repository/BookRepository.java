package rs.levi9.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.levi9.library.model.Book;
import rs.levi9.library.model.LibraryUser;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    Book findBookByIsbn13(String isbn);
    List<Book> findAllByAuthorNameContainingIgnoreCaseOrTitleContainingIgnoreCase(String query, String q);
    //List<Book> findAllByTitleContainingIgnoreCase(String title);
}