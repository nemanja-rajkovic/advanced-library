INSERT INTO role (role_type) VALUES ('ROLE_ADMIN');
INSERT INTO role (role_type) VALUES ('ROLE_USER');

insert into library_user (username, email, password, email_confirmed, name, gender, description, blocked, date_joined)
 values ("admin", "admin@lib.com", "admin", 1, "Admina Adminovic", true, "This is description bla bla bla bla bla", false, "2016-05-26");

insert into library_user (username, email, password, email_confirmed, name, gender, description, blocked, email_notifications_allowed, date_joined)
values ("user", "libappuser1@gmail.com", "user", 1, "Tamara Bilinac", true, "I like this app very much. Thanks to the people that made it happen! You are real heroes.", false, true, "2017-04-16");

INSERT INTO library_user_roles(library_user_id,role_id)VALUES(1, 1);
INSERT INTO library_user_roles(library_user_id,role_id)VALUES(1, 2);
