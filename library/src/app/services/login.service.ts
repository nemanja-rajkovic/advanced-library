import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do'
import 'rxjs';

export class AuthUser {
  id: number;
  username: string;
  roles: string[];
  email: string;
  emailConfirmed: boolean;
  blocked: boolean;
  name : string;
  lastname : string;
}

@Injectable()
export class LoginService {

  user: AuthUser;
  private authenticated = false;
  private headers;
  public url = "//localhost:8080/";
 // public url = "http://99649a56.ngrok.io/";
  constructor(private http: HttpClient, private router: Router) { }

  login(username: string, password: string) {
    this.user = new AuthUser();
    const base64Credential = btoa(username + ':' + password);
    const headers = new HttpHeaders({ authorization: 'Basic ' + base64Credential });

    return this.http.get<any>(this.url + "auth/user", { headers: headers }).do(data => {
      this.user = data;
      this.headers = headers;
      this.authenticated = true;
    });
  }

  public getUser(id : number) : Observable<any>{
   return this.http.get(this.url +"user/"+id, { headers: this.headers });
  }

  public captchaCheck(captcha: String): Observable<any> {
    return this.http.get(this.url +"user/captcha/" + captcha);
  }

  public getAuthHeaders() {
    return this.headers;
  }

  public getAuthUser() {
    return this.user;
  }

  public isUserAuth() {
    return this.authenticated;
  }

  hasRoleAdmin() {
    if (this.user != null && this.user.roles != null) {
      if (this.user) {
        return this.user.roles.includes('ROLE_ADMIN');
      }
    }
  }

  logout() {
    this.authenticated = false;
    this.user = null;
    this.headers = null;
    this.router.navigate(['/login']);
  }
}
