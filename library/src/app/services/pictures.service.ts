import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';
import { Observable } from 'rxjs';

@Injectable()
export class PicturesService {

  constructor(private http: HttpClient, public loginService: LoginService) { }

  uploadImage(profileImage: File, id: number): Observable<any> {
    const formData = new FormData();
    formData.append('profileImage', profileImage);
    return this.http.post(this.loginService.url + 'user/image/' + id, formData, { headers: this.loginService.getAuthHeaders() });
  }

}
