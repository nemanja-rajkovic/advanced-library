import { Injectable } from '@angular/core';
import { LoginService } from './login.service';
import { BookComment } from '../model/dto/book-comment';
import { CommentReply } from '../model/dto/comment-reply';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NotificationService {

  constructor(private http: HttpClient, public loginService: LoginService) { }

  notifyUsers(comment: BookComment): Observable<any> {
    return this.http.post(this.loginService.url + "notification/notify-users", comment, { headers: this.loginService.getAuthHeaders() });
  }
  notifyUserForReply(comment: CommentReply): Observable<any> {
    return this.http.post(this.loginService.url + "notification/notify-user", comment, { headers: this.loginService.getAuthHeaders() });
  }

  getNotifications(id: number): Observable<any> {
    return this.http.get(this.loginService.url + "notification/" + id, { headers: this.loginService.getAuthHeaders() });
  }

  deleteNotification(userId: number, id: number): Observable<any> {
    return this.http.delete(this.loginService.url + "notification/" + userId + "/" + id, { headers: this.loginService.getAuthHeaders() });
  }

  updateNotification(userId: number, id: number): Observable<any> {
    return this.http.get(this.loginService.url + "notification/" + userId + "/" + id, { headers: this.loginService.getAuthHeaders() });
  }

}
