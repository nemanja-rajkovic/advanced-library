import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LibraryUser } from '../model/library-user.model';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable()
export class LibraryUserService {

  constructor(private http: HttpClient, private loginService: LoginService) { }

  forgotPassword(user: LibraryUser): Observable<any> {
    return this.http.post(this.loginService.url + 'user/forgot-password', user);
  }

  getProfiles(): Observable<any> {
    return this.http.get(this.loginService.url + 'user/get-all', { headers: this.loginService.getAuthHeaders() });
  }

  getProfilesByQuery(str: string): Observable<any> {
    return this.http.get(this.loginService.url + 'user/get-all/' + str, { headers: this.loginService.getAuthHeaders() });
  }

  unblockUser(id: number): Observable<any> {
    return this.http.get(this.loginService.url + 'user/unblock/' + id, { headers: this.loginService.getAuthHeaders() });
  }

  block(user: LibraryUser): Observable<any> {
    return this.http.put(this.loginService.url + 'user/block', user, { headers: this.loginService.getAuthHeaders() });
  }

  delete(id: number) {
    return this.http.delete(this.loginService.url + 'user/' + id, { headers: this.loginService.getAuthHeaders() });
  }

  updateUser(user: LibraryUser): Observable<any> {
    return this.http.put(this.loginService.url + 'user', user, { headers: this.loginService.getAuthHeaders() });
  }

  changePassword(user: LibraryUser): Observable<any> {
    return this.http.put(this.loginService.url + 'user/change-password', user, { headers: this.loginService.getAuthHeaders() });
  }

}
