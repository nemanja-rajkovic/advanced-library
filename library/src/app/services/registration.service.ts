import { Injectable } from '@angular/core';
import { LibraryUser } from '../model/library-user.model';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';

@Injectable()
export class RegistrationService {

  constructor(private http: HttpClient, private loginService : LoginService) { }

  public register(user: LibraryUser): Observable<any> {
    return this.http.post(this.loginService.url + "user/register", user);
  }
  public captchaCheck(captcha: String): Observable<any> {
    return this.http.get(this.loginService.url + "user/captcha/" + captcha);
  }
}