import { Injectable } from '@angular/core';
import { LoginService } from './login.service';
import { Observable } from 'rxjs';
import { BookComment } from '../model/dto/book-comment';
import { HttpClient } from '@angular/common/http';
import { CommentReport } from '../model/dto/comment-report';
import { CommentReply } from '../model/dto/comment-reply';
@Injectable()
export class CommentService {

  constructor(private http: HttpClient, public loginService: LoginService) { }

  ///book/save-comment  BookComment bookComment, get-comment/{userId}/{bookId}, edit-comment
  saveComment(bookComment: BookComment): Observable<any> {
    return this.http.post(this.loginService.url + "book/save-comment", bookComment, { headers: this.loginService.getAuthHeaders() });
  }

  getComments(bookId: number): Observable<any> {
    return this.http.get(this.loginService.url + "book/get-comment/" + bookId, { headers: this.loginService.getAuthHeaders() });
  }

  getCommentById(commentId: number): Observable<any> {
    return this.http.get(this.loginService.url + "book/comment/" + commentId, { headers: this.loginService.getAuthHeaders() });
  }

  updateComment(bookComment: BookComment): Observable<any> {
    return this.http.put(this.loginService.url + "book/edit-comment", bookComment, { headers: this.loginService.getAuthHeaders() });
  }

  deleteComment(id: number, bookId: number): Observable<any> {
    return this.http.delete(this.loginService.url + "book/delete-comment/" + id + "/" + bookId, { headers: this.loginService.getAuthHeaders() });
  }

  reportComment(commentReport: CommentReport): Observable<any> {
    return this.http.post(this.loginService.url + "notification/report-comment", commentReport, { headers: this.loginService.getAuthHeaders() });
  }

}
