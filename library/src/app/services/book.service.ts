import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../model/book';
import { BookComment } from '../model/dto/book-comment';
import { Comment } from '../model/comment';
import { LoginService } from './login.service';

@Injectable()
export class BookService {

  constructor(private http: HttpClient, public loginService: LoginService) { }

  getOneWithUserId(id: number, userId: number): Observable<any> {
    return this.http.get(this.loginService.url + 'book/' + id + '/' + userId, { headers: this.loginService.getAuthHeaders() });
  }

  getOneWithBookId(id: number): Observable<any> {
    return this.http.get(this.loginService.url + 'book/' + id, { headers: this.loginService.getAuthHeaders() });
  }

  getReadBooks(id: number): Observable<any> {
    return this.http.get(this.loginService.url + 'user/read-books/' + id, { headers: this.loginService.getAuthHeaders() });
  }

  getToReadBooks(id: number): Observable<any> {
    return this.http.get(this.loginService.url + 'user/to-read-books/' + id, { headers: this.loginService.getAuthHeaders() });
  }

  getReadingBooks(id: number): Observable<any> {
    return this.http.get(this.loginService.url + 'user/reading-books/' + id, { headers: this.loginService.getAuthHeaders() });
  }

  public postReadBook(book: Book): Observable<any> {
    return this.http.post(this.loginService.url + 'user/post-read', book, { headers: this.loginService.getAuthHeaders() });
  }

  public postBook(book: Book): Observable<any> {
    return this.http.post(this.loginService.url + 'book', book, { headers: this.loginService.getAuthHeaders() });
  }
  /**
   * This method will move user's book to read list.
   * @param id - id of the book to be moved to read list.
   */
  public moveBookToReadList(book: Book): Observable<any> {
    return this.http.put(this.loginService.url + 'book/move-to-read-list', book, { headers: this.loginService.getAuthHeaders() });
  }
  // http://localhost:8080/book/move-to-reading-list
  /**
   * This method will move user's book to reading list.
   * @param id - id of the book to be moved to read list.
   */
  public moveBookToReadingList(book: Book): Observable<any> {
    return this.http.put(this.loginService.url + 'book/move-to-reading-list', book, { headers: this.loginService.getAuthHeaders() });
  }

  /**
  * This method will move user's book to to-read list.
  * @param id - id of the book to be moved to read list.
  */
  public moveBookToToReadList(book: Book): Observable<any> {
    return this.http.put(this.loginService.url + 'book/move-to-to-read-list', book, { headers: this.loginService.getAuthHeaders() });
  }

  public getBookForSocialShare(id: number): Observable<any> {
    return this.http.get(this.loginService.url + 'book/social-share/' + id);
  }
  /**
   * This method will retrieve list of all books in database.
   */
  getAllBooks(): Observable<any> {
    return this.http.get(this.loginService.url + 'book', { headers: this.loginService.getAuthHeaders() });
  }

  getAllUserBooks(userId: number): Observable<any> {
    return this.http.get(this.loginService.url + 'user/get-all-books/' + userId, { headers: this.loginService.getAuthHeaders() });
  }

  searchBooksDB(query: string): Observable<any> {
    return this.http.get(this.loginService.url + 'book/get-from-db/' + query, { headers: this.loginService.getAuthHeaders() });
  }

  public put(book: Book): Observable<any> {
    return this.http.put(this.loginService.url + 'book/', book, { headers: this.loginService.getAuthHeaders() });
  }

  public updateBookStatus(book: Book): Observable<any> {
    return this.http.put(this.loginService.url + 'user/update-status', book, { headers: this.loginService.getAuthHeaders() });
  }

  public deleteBook(bookId, userId): Observable<any> {
    return this.http.delete(this.loginService.url + 'book/delete/' + bookId + '/' + userId, { headers: this.loginService.getAuthHeaders() });
  }



}
