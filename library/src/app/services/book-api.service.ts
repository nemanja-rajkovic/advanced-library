import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class BookApiService {

  constructor(private http: HttpClient) { }

  public searchBooksByName(name: string): Observable<any> {
    return this.http.get('https://www.googleapis.com/books/v1/volumes?q=' + name);
  }
  public getBookByUrl(url: string): Observable<any> {
    return this.http.get(url);
  }

}
