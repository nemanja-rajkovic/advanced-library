import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: '<re-captcha (resolved)="resolved($event)" siteKey="YOUR_SITE_KEY"></re-captcha>',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
