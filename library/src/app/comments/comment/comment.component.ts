import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Comment } from '../../model/comment';
import { Router } from '@angular/router';
import { BookComment } from '../../model/dto/book-comment';
import { CommentService } from '../../services/comment.service';
import { LoginService } from '../../services/login.service';
import { CommentReport } from '../../model/dto/comment-report';
import { CommentReply } from '../../model/dto/comment-reply';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input("comment") public comment: Comment;
  @Input("bookId") public bookId: number;

  public editing = false;
  public editButtonText = "Edit";
  public commText: string;
  public replying = false;
  public replyText: string;
  public replyTo: string;
  public replyComment: BookComment;
  public comm: Comment;
  public reported = false;
  @Output() uploaded = new EventEmitter<any>();
  
  constructor(public loginService: LoginService, private router: Router, private commentService: CommentService, private notifService : NotificationService) { }

  ngOnInit() {
    this.commText = this.comment.text;
   }

  deleteComment() {
    return this.commentService.deleteComment(this.comment.id, this.bookId).subscribe(
      (data) => {
        setTimeout(() => {
          this.uploaded.emit('complete');
        }, 500);
      },
      (error) => {
        console.log(error);
      }
    )
  }

  setEdit() {
    if (this.editing === false) {
      this.editing = true;
      this.editButtonText = "Cancel";
    } else {
      this.editing = false;
      this.editButtonText = "Edit";
      this.commText = this.comment.text;
    }
  }

  saveComment() {
    let c = new BookComment();
    c.bookId = this.bookId;
    c.text = this.commText;
    c.commentId = this.comment.id;

    return this.commentService.updateComment(c).subscribe(
      (data) => {
        setTimeout(() => {
          this.uploaded.emit('complete');
        }, 500);
      },
      (error) => {
        console.log(error);
      }
    )
  }

  setReplying() {
    this.replying = !this.replying;
  }

  replyToComment() {
    this.createReplyComment();
    this.postComment(this.replyComment);
  }

  getReplyTo() {
    return this.comment.userName + "'s: " + this.comment.text;
  }

  postComment(comm: BookComment) {
    return this.commentService.saveComment(comm).subscribe(
      (data) => {
        this.comm = data;
        this.notifyUserForReply(this.convertBookCommentToCommentReply(this.replyComment));
        setTimeout(() => {
          this.uploaded.emit('complete');
        }, 500);

      },
      (error) => {
        console.log(error);
      }
    )
  }

  public reportComment() {
    let report = new CommentReport(this.comment.id, this.comment.userId, this.loginService.user.id, this.bookId);
    return this.commentService.reportComment(report).subscribe(
      (data) => {
        this.reported = true;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  public notifyUserForReply(comment: CommentReply) {
    comment.commentId = this.comm.id;
    this.notifService.notifyUserForReply(comment).subscribe(
      (data) => {
      //  this.router.navigate(['book-list/1']);
      },
      (error) => {
        console.log(error);
      }
    )
  }

  convertBookCommentToCommentReply(comm: BookComment) {
    let comment = new CommentReply();
    comment.commentUserId = this.replyComment.commentUserId;
    comment.bookId = this.replyComment.bookId;
    comment.commentOwnerId = this.comment.userId;
    comment.userName = this.loginService.user.name;
    comment.commentId = this.replyComment.commentId;
    return comment;
  }

  createReplyComment() {
    this.replyComment = new BookComment();
    this.replyComment.textToReply = this.getReplyTo();
    this.replyComment.text = this.replyText;
    this.replyComment.commentUserId = this.loginService.user.id;
    this.replyComment.userName = this.loginService.user.name;
    this.replyComment.bookId = this.bookId;
  }

}