import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookComment } from '../../model/dto/book-comment';
import { Router } from '@angular/router';
import { CommentService } from '../../services/comment.service';
import { LoginService } from '../../services/login.service';
import { Comment } from '../../model/comment';
import { NotificationService } from '../../services/notification.service';


@Component({
  selector: 'app-write-comment',
  templateUrl: './write-comment.component.html',
  styleUrls: ['./write-comment.component.css']
})
export class WriteCommentComponent implements OnInit {

  public text: string;
  @Input("bookId") public bookId: number;
  public commenterId: number;
  public comm: Comment;
  constructor(private router: Router, private loginService: LoginService, private commentService: CommentService, private notifService: NotificationService) { }
  @Output() uploaded = new EventEmitter<any>();

  ngOnInit() {
  }

  postComment() {

    let comment = new BookComment();
    return this.commentService.saveComment(this.createComment()).subscribe(
      (data) => {
        this.comm = data;
        this.notifyUsers();
        setTimeout(() => {
          this.text = "";
          this.uploaded.emit('complete');
        }, 500);

      },
      (error) => {
        console.log(error);
      }
    )
  }

  createComment() {

    let comment = new BookComment();
    comment.commentUserId = this.loginService.user.id;
    comment.text = this.text;
    comment.userName = this.loginService.user.name;
    comment.bookId = this.bookId;
    return comment;
  }

  notifyUsers() {
    let c = this.createComment();
    c.commentId = this.comm.id
    return this.notifService.notifyUsers(c).subscribe(
      (data) => { },
      (error) => { console.log(error) }
    )
  }

}
