import { Component, OnInit } from '@angular/core';
import { CommentService } from '../../services/comment.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Comment } from '../../model/comment';
import { NotificationService } from '../../services/notification.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-reported-comment',
  templateUrl: './reported-comment.component.html',
  styleUrls: ['./reported-comment.component.css']
})
export class ReportedCommentComponent implements OnInit {

  public commId : number;
  public bookId : number;
  public sub : any;
  public comment : Comment;
  public notificationId : number;

  constructor(private route : ActivatedRoute, private router: Router, private commentService: CommentService, private notifService : NotificationService, private loginService : LoginService) { }

  ngOnInit() {
    this.getCommentIdFomUrl();
    this.getBookIdFomUrl();
    this.getNotificationIdFomUrl();
  }

  public getCommentById(id: number) {
    this.commentService.getCommentById(id).subscribe(
      (data) => {
        this.comment = data;
      },
      (error) => console.log(error)
    )
  }

  deleteComment() {
    return this.commentService.deleteComment(this.commId, this.bookId).subscribe(
      (data) => {
        this.deleteNotification();
        this.router.navigate(['home']);
      },
      (error) => {
        console.log(error);
      }
    )
  }

  deleteNotification() {

    this.notifService.deleteNotification(this.loginService.user.id, this.notificationId).subscribe(
      (data) => { },
      (error) => {
        console.log(error);
      }
    )
  }
  
  public getCommentIdFomUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.commId = +params['commId'];
      this.getCommentById(this.commId);
    });
  }

  public getBookIdFomUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.bookId = +params['bookId'];
    });
  }

  public getNotificationIdFomUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.notificationId = +params['notId'];
    });
  }

 
}
