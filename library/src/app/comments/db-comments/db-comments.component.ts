import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../../model/comment';

@Component({
  selector: 'app-db-comments',
  templateUrl: './db-comments.component.html',
  styleUrls: ['./db-comments.component.css']
})
export class DbCommentsComponent implements OnInit {

  @Input("comment") public comment: Comment;

  constructor() { }

  ngOnInit() { }

}
