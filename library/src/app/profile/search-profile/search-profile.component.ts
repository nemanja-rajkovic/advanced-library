import { Component, OnInit } from '@angular/core';
import { LibraryUser } from '../../model/library-user.model';
import { BookService } from '../../services/book.service';
import { Router } from '@angular/router';
import { LibraryUserService } from '../../services/library-user.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-search-profile',
  templateUrl: './search-profile.component.html',
  styleUrls: ['./search-profile.component.css']
})
export class SearchProfileComponent implements OnInit {

  public searchString: string;
  public profiles: LibraryUser[] = [];
  public alternateUrl = "https://cdn1.iconfinder.com/data/icons/user-pictures/100/unknown-512.png";
  public isSearched = false;

  constructor(private loginService: LoginService, private router: Router, private userService: LibraryUserService) { }

  ngOnInit() {
  }

  /**
   * This method will search for users profiles.
   */
  public searchProfiles() {
    if (this.searchString === null || this.searchString === undefined || this.searchString === '') {
      return this.userService.getProfiles().subscribe(
        (data) => {
          this.profiles = data;
          this.removeLoggedUserFromList();
        },
        (error) => console.log(error)
      );
    } else {
      this.isSearched = true;
      return this.userService.getProfilesByQuery(this.searchString).subscribe(
        (data) => {
          this.profiles = data;
          this.removeLoggedUserFromList();
        },
        (error) => console.log(error)
      );
    }
  }

  seeProfile(i: number) {
    this.router.navigate(['visit-profile/' + this.profiles[i].id]);
  }

  removeLoggedUserFromList() {
    for (let i = 0; i < this.profiles.length; i++) {
      if (this.profiles[i].id === this.loginService.user.id) {
        this.profiles.splice(i, 1);
      }
    }
  }

}
