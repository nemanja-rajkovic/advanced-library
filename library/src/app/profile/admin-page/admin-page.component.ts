import { Component, OnInit } from '@angular/core';
import { LibraryUser } from '../../model/library-user.model';
import { LibraryUserService } from '../../services/library-user.service';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  users: LibraryUser[];
  isBlocked: boolean;
  user: LibraryUser;
  selectedUser: LibraryUser;
  username = "default";
  allowCalendar: boolean = false;
  blockedUntil: Date;
  click: boolean;
  minDate: string;
  today = new Date();

  constructor(private userService: LibraryUserService, private router: Router, private loginService: LoginService) { }

  ngOnInit() {
    if (!this.loginService.hasRoleAdmin()) {
      this.router.navigate(['login']);
      return;
    }
    this.selectedUser = new LibraryUser();
    this.getUserList();
    this.minDate = this.updateMinimumDate();
  }

  getUserList() {
    this.userService.getProfiles().subscribe(data => {
      this.users = data;
    },
      (error) => {
        console.log(error);
      });
  }

  onUserDelete(user: LibraryUser) {
    this.username = user.username;
    this.selectedUser = user;
  }

  onUserDeleteSubmit() {
    this.userService.delete(this.selectedUser.id).subscribe(
      () => {
        this.selectedUser = null;
      },
      (error) => { console.error(error) },
      () => this.getUserList()
    );
  }

  setAllowCalendarTrue() {
    this.allowCalendar = true;
    this.click = true;
  }

  setAllowCalendarFalse() {
    this.allowCalendar = false;
    this.click = true;
  }

  onBlockUser(user: LibraryUser) {
    this.username = user.username;
    this.user = user;
  }

  blockUserOk() {
    if (!this.allowCalendar) {
      this.user.blocked = true;
      this.user.blockedUntil = null;
    } else {
      this.user.blocked = true;
      this.user.blockedUntil = this.blockedUntil;
    }
    this.userService.block(this.user).subscribe((data) => {
    },
      (error) => {
        console.log(error);
      }
    );
  }

  onUnblockUser(user: LibraryUser) {
    this.username = user.username;
    this.user = user;

  }

  unblockUserOk() {
    this.user.blocked = false;
    this.user.blockedUntil = null;
    this.userService.unblockUser(this.user.id).subscribe(
      (data) => { },
      (error) => { console.log(error); }
    );
    this.allowCalendar = null;
  }

  updateMinimumDate() {
    let myTimeStamp = this.today.setDate(this.today.getDate() + 1);
    const datePipe = new DatePipe('en-US');
    return datePipe.transform(myTimeStamp, 'yyyy-MM-dd');
  }

  timeStampToDate() {
    let myTimeStamp = this.today.setDate(this.today.getDate() + 365);
    const datePipe = new DatePipe('en-US');
    return new Date(datePipe.transform(myTimeStamp, 'yyyy-MM-dd'));
  }

}