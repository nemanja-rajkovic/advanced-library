import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../../services/book.service';
import { LibraryUser } from '../../model/library-user.model';
import { LoginService } from '../../services/login.service';
import { Book } from '../../model/book';

@Component({
  selector: 'app-visit-profile',
  templateUrl: './visit-profile.component.html',
  styleUrls: ['./visit-profile.component.css']
})
export class VisitProfileComponent implements OnInit {

  id: number;
  private sub: any;
  public user: LibraryUser;
  public alternateUrl = "https://cdn1.iconfinder.com/data/icons/user-pictures/100/unknown-512.png";
  public books: Book[] = [];
  public notEmpty = false;

  constructor(private route: ActivatedRoute, private router: Router, private bookService: BookService, private loginService: LoginService) { }

  ngOnInit() {
    this.getParamFromUrl();
    this.getOne(this.id);
  }

  /**
   * This method will retrieve parameter from url such as ID and convert it to number that we need
   * to retrieve the account we want.
   */
  public getParamFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
    this.bookService.getAllUserBooks(this.id).subscribe(
      (data) => {
        this.books = data;
        for (let i = 0; i < this.books.length; i++) {
          if (this.books[i].bookIsPrivate === true) {
            this.books.splice(i, 1);
            i--;
          }
          if (this.books[i].userRating === 0 || this.books[i].userRating === null) {
            this.books.splice(i, 1);
            i--;
          }
        }
        if (this.books.length > 0) {
          this.notEmpty = true;
        }
        this.books.splice(5);
      },
      (error) => console.log(error)
    );
  }

  /**
   * This method will get one Book by id from BookService.
   * @param id - id of the book to be retrieved.
   */
  getOne(id: number) {
    this.loginService.getUser(id).subscribe(
      (data) => {
        this.user = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  showUsersToReadBooks() {
    this.router.navigate(['book-list/' + this.id + '/3']);
  }

  showUsersReadBooks() {
    this.router.navigate(['book-list/' + this.id + '/1']);
  }

  showUsersReadingBooks() {
    this.router.navigate(['book-list/' + this.id + '/2']);
  }

  previewBook(id: number) {
    this.router.navigate(['preview-books-db/' + id]);
  }

}
