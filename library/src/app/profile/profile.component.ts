import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../services/book.service';
import { LibraryUser } from '../model/library-user.model';
import { PicturesService } from '../services/pictures.service';
import { LoginService } from '../services/login.service';
import { Book } from '../model/book';
import { LibraryUserService } from '../services/library-user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public id: number;
  public user: LibraryUser;
  public url: any;
  public alternateUrl = "https://cdn1.iconfinder.com/data/icons/user-pictures/100/unknown-512.png";
  public file: File;
  public imagePath: any;
  public books: Book[] = [];
  public description: string;
  public editing = false;
  public editButtonText: string;
  public hover = false;
  public notEmpty = false;
  public changingPass = false;
  public wrongPic = false;
  public newPasword: string;
  @ViewChild('newpass') input;

  constructor(private route: ActivatedRoute, private userService: LibraryUserService, private router: Router, private bookService: BookService, private loginService: LoginService, private picService: PicturesService) { }

  ngOnInit() {
    this.id = this.loginService.user.id;
    this.getUsersData();
  }

  public getUsersData() {
    this.loginService.getUser(this.id).subscribe(
      (data) => {
        this.user = data;
        this.getBooks();
        this.books.slice(5);
        this.description = this.user.description;
        if (this.user.pictureUrl !== undefined && this.user.pictureUrl !== null) {
          this.url = this.user.pictureUrl;
        }
      },
      (error) => console.log(error)
    );
  }

  public getGender() {
    return this.user.gender ? "Male" : "Female";
  }


  setImage(files: FileList) {
    this.file = files.item(0);

    if (this.file.type === "image/jpeg" || this.file.type === "image/png") {
      return this.picService.uploadImage(this.file, this.loginService.user.id).subscribe(
        () => {
          this.wrongPic = false;
          this.router.navigate([this.router.url]);
          this.ngOnInit();
        },
        (error) => {
        }
      );
    } else {
      this.wrongPic = true;
    }

  }

  saveDescription() {
    this.user.description = this.description;
    this.userService.updateUser(this.user).subscribe(
      () => {
        this.editing = false;
      }
    );
  }

  cancelEdit() {
    this.editing = false;
    this.description = this.user.description;
  }

  setEdit() {
    this.editing = true;
    this.editButtonText = "Cancel Editing";
  }

  public getBooks() {
    this.bookService.getAllUserBooks(this.id).subscribe(
      (data) => {
        this.books = data;
        for (let i = 0; i < this.books.length; i++) {
          if (this.books[i].userRating === 0 || this.books[i].userRating === null) {
            this.books.splice(i, 1);
            i--;
          }
        }
        if (this.books.length > 0) {
          this.notEmpty = true;
        }
      },
      (error) => console.log(error)
    );
  }

  public previewBook(id: number) {
    this.router.navigate(['preview-book/' + id]);
  }

  toReadBooks() {
    this.router.navigate(['book-list/' + 3]);
  }

  readBooks() {
    this.router.navigate(['book-list/' + 1]);
  }

  readingBooks() {
    this.router.navigate(['book-list/' + 2]);
  }

  changePassword() {
    let user = new LibraryUser();
    user.password = this.newPasword;
    user.id = this.user.id;

    this.userService.changePassword(user).subscribe(
      () => {
        location.reload();
      },
      (error) => console.log(error)
    );
  }

  myFunction() {
    let x = document.getElementById("newpass");
    if (this.input.nativeElement.type === "password") {
      this.input.nativeElement.type = "text";
    } else {
      this.input.nativeElement.type = "password";
    }
  }

}
