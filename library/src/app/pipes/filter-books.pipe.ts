import { Pipe, PipeTransform } from '@angular/core';
import { Book } from '../model/book';

@Pipe({
  name: 'filterBooks'
})
export class FilterBooksPipe implements PipeTransform {
  public genre : boolean;

  transform(books: Book[], criteria: string, value: string): Book[] {
    if(criteria === "author") {  
      if(value && value != 'all') {
        return books.filter(book => {
          if(book.author.name.toLowerCase().indexOf(value.toLowerCase()) === -1) {
            return false;
          } return true;
        });
      }
    } else if(criteria === "title") {  
      if(value && value != 'all') {
          return books.filter(book => {
            if(book.title.toLowerCase().indexOf(value.toLowerCase()) === -1) {
              return false;
            } return true;
          });
      }
    } else if(criteria === "genre") {  
        if(value && value != 'all') {
          return books.filter(book => {
            for(let i = 0; i<book.genreList.length; i++) {
              if(book.genreList[i].genre.toLowerCase().indexOf(value.toLowerCase()) === -1) {
                this.genre = false;
              } else { 
                return true; 
              } 
            } return this.genre;
          });
        }
    }
    return books;
  }

}
