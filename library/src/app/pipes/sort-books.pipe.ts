import { Pipe, PipeTransform } from '@angular/core';
import { Book } from '../model/book';

@Pipe({
  name: 'sortBooks'
})
export class SortBooksPipe implements PipeTransform {

  transform(books: Book[], sortBy: string, type: string): Book[] {
    if(type == "asc") {
      if(sortBy == "author") {
          books.sort((a, b) => {
          if (a.author.name<b.author.name) {
            return -1;
          }
          if (a.author.name>b.author.name) {
            return 1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "genre") {
        books.sort((a, b) => {
          if (a.genreList[0].genre <b.genreList[0].genre) {
            return -1;
          }
          if (a.genreList[0].genre>b.genreList[0].genre) {
            return 1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "title") {
        books.sort((a, b) => {
          if (a.title<b.title) {
            return -1;
          }
          if (a.title>b.title) {
            return 1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "publisher") {
        books.sort((a, b) => {
          if (a.publisher<b.publisher) {
            return -1;
          }
          if (a.publisher>b.publisher) {
            return 1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "rating") {
        books.sort((a, b) => {
          if (a.userRating<b.userRating) {
            return -1;
          }
          if (a.userRating>b.userRating) {
            return 1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "globalRating") {
        books.sort((a, b) => {
          if (a.globalRating.votes<b.globalRating.votes) {
            return -1;
          }
          if (a.globalRating.votes>b.globalRating.votes) {
            return 1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "commentsNumber") {
        books.sort((a, b) => {
          if (a.comments.length<b.comments.length) {
            return -1;
          }
          if (a.comments.length>b.comments.length) {
            return 1;
          } 
          else {
            return 0;
          }
          });
      }
    }
    if(type == "desc") {
      if(sortBy == "author") {
          books.sort((a, b) => {
          if (a.author.name<b.author.name) {
            return 1;
          }
          if (a.author.name>b.author.name) {
            return -1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "genre") {
        books.sort((a, b) => {
          if (a.genreList[0].genre <b.genreList[0].genre) {
            return 1;
          }
          if (a.genreList[0].genre>b.genreList[0].genre) {
            return -1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "title") {
        books.sort((a, b) => {
          if (a.title<b.title) {
            return 1;
          }
          if (a.title>b.title) {
            return -1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "publisher") {
        books.sort((a, b) => {
          if (a.publisher<b.publisher) {
            return 1;
          }
          if (a.publisher>b.publisher) {
            return -1;
          } 
          else {
            return 0;
          }
          });
      }  else if(sortBy == "rating") {
        books.sort((a, b) => {
          if (a.userRating<b.userRating) {
            return 1;
          }
          if (a.userRating>b.userRating) {
            return -1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "globalRating") {
        books.sort((a, b) => {
          if (a.globalRating.votes<b.globalRating.votes) {
            return 1;
          }
          if (a.globalRating.votes>b.globalRating.votes) {
            return -1;
          } 
          else {
            return 0;
          }
          });
      } else if(sortBy == "commentsNumber") {
        books.sort((a, b) => {
          if (a.comments.length<b.comments.length) {
            return 1;
          }
          if (a.comments.length>b.comments.length) {
            return -1;
          } 
          else {
            return 0;
          }
          });
      }
    }
    return books;
  }

}
