import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EqualValidator } from './templates/registration/equal-validator.directive';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { JwSocialButtonsModule } from 'jw-angular-social-buttons';
import { AppComponent } from './app.component';
import { LogInComponent } from './templates/log-in/log-in.component';
import { RegistrationComponent } from './templates/registration/registration.component';
import { AppRoutingModule } from './app-routing.module';
import { RecaptchaModule } from 'ng-recaptcha';
import { FormsModule } from '@angular/forms';
import { LibraryUserComponent } from './profile/user/library-user.component';
import { HomeComponent } from './templates/home/home.component';
import { HeaderComponent } from './templates/header/header.component';
import { BookComponent } from './book/single-book/book.component';
import { BooksearchComponent } from './book/booksearch/booksearch.component';
import { ApiBookPreviewComponent } from './book/api-book-preview/api-book-preview.component';
import { BookListComponent } from './book/book-list/book-list.component';
import { PreviewReadComponent } from './book/preview-read/preview-read.component';
import { SortBooksPipe } from './pipes/sort-books.pipe';
import { SearchBooksFromDbComponent } from './book/search-books-from-db/search-books-from-db.component';
import { PreviewDbBooksComponent } from './book/preview-db-books/preview-db-books.component';
import { ProfileComponent } from './profile/profile.component';
import { FilterBooksPipe } from './pipes/filter-books.pipe';
import { SearchProfileComponent } from './profile/search-profile/search-profile.component';
import { VisitProfileComponent } from './profile/visit-profile/visit-profile.component';
import { SocialShareComponent } from './book/social-share/social-share.component';
import { AuthGuardService } from './services/auth-guard.service';
import { BookService } from './services/book.service';
import { BookApiService } from './services/book-api.service';
import { CommentService } from './services/comment.service';
import { LibraryUserService } from './services/library-user.service';
import { LoginService } from './services/login.service';
import { PicturesService } from './services/pictures.service';
import { RegistrationService } from './services/registration.service';
import { CommentComponent } from './comments/comment/comment.component';
import { WriteCommentComponent } from './comments/write-comment/write-comment.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { AdminPageComponent } from './profile/admin-page/admin-page.component';
import { LogoutComponent } from './templates/logout/logout.component';
import { ReportedCommentComponent } from './comments/reported-comment/reported-comment.component';
import { NotificationService } from './services/notification.service';
import { DbCommentsComponent } from './comments/db-comments/db-comments.component';


@NgModule({
  declarations: [
    EqualValidator,
    AppComponent,
    LogInComponent,
    RegistrationComponent,
    LibraryUserComponent,
    HomeComponent,
    HeaderComponent,
    BookComponent,
    BooksearchComponent,
    ApiBookPreviewComponent,
    BookListComponent,
    PreviewReadComponent,
    SortBooksPipe,
    FilterBooksPipe,
    SearchBooksFromDbComponent,
    PreviewDbBooksComponent,
    ProfileComponent,
    SearchProfileComponent,
    VisitProfileComponent,
    CommentComponent,
    WriteCommentComponent,
    SocialShareComponent,
    AdminPageComponent,
    LogoutComponent,
    ReportedCommentComponent,
    DbCommentsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RecaptchaModule.forRoot(),
    ScrollToModule.forRoot(),
    FormsModule,
    HttpClientModule,
    HttpModule,
    JwSocialButtonsModule
  ],
  providers: [
    AuthGuardService,
    BookApiService,
    BookService,
    CommentService,  
    LibraryUserService,
    LoginService,
    PicturesService,
    RegistrationService,
    NotificationService
  ]
  ,
  bootstrap: [AppComponent]
})
export class AppModule { }
