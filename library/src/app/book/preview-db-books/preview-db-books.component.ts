import { Component, OnInit } from '@angular/core';
import { Book } from '../../model/book';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../../services/book.service';
import { CommentService } from '../../services/comment.service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-preview-db-books',
  templateUrl: './preview-db-books.component.html',
  styleUrls: ['./preview-db-books.component.css']
})
export class PreviewDbBooksComponent implements OnInit {

  public state: number;
  private sub: any;
  private id: number;
  public book: Book;
  public currUserIsOwner: boolean;
  public private: boolean;
  public commentList: Comment[] = [];

  constructor(private route: ActivatedRoute, private router: Router, private bookService: BookService, private loginService: LoginService, private commentService: CommentService) { }

  ngOnInit() {
    this.getParamFromUrl();
  }

  /** 
   * This method will retrieve id of the book from url.
   */
  public getParamFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.getBookByID(this.id);
    });
  }

  public getComments() {
    return this.commentService.getComments(this.id).subscribe(
      (data) => {
        this.commentList = data;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  /**
   * This method will retreive book by id from DB.
   * @param id - id of the book to be retrieved.
   */
  public getBookByID(id: number) {
    this.bookService.getOneWithBookId(id).subscribe(
      (data) => {
        this.book = data;
        this.getComments();
      },
      (error) => console.log(error)
    )
  }

  /**
     * This method will save the book from the google's book-api.
     */
  public saveBook(state : string) {
    this.book.userId = this.loginService.user.id;
    this.book.state = state;
    this.bookService.postBook(this.book).subscribe(
      () => {},
      //on success write a message that user has succesfully saved a book.
      (error) => console.log("Error: " + error),

    )
  }

  /**
    * This method will move a book to user's read list.
    */
  public moveToReadList() {
    this.saveBook("READ");
    this.router.navigate(['book-list/' + 1]);
  }


  /**
   * This method will move book to user's reading list.
   */
  public moveToReadingList() {
    this.saveBook("READING");
    this.router.navigate(['book-list/' + 2]);
  }

  
  /**
   * This method will move book to user's to read list.
   */
  public moveToToReadList() {
    this.saveBook("TOREAD");
    this.router.navigate(['book-list/' + 3]);
  }

}
