import { Component, OnInit } from '@angular/core';
import { Book } from '../../model/book';
import { BookService } from '../../services/book.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-books-from-db',
  templateUrl: './search-books-from-db.component.html',
  styleUrls: ['./search-books-from-db.component.css']
})
export class SearchBooksFromDbComponent implements OnInit {

  public book: Book;
  public books: Book[] = [];
  public searchByTitle = true;
  public searchByAuthor = false;
  public searchString: string;
  public isSearched = false;
  constructor(private service: BookService, private router: Router) { }

  ngOnInit() {
  }

  public searchBooks() {
    if(this.searchString === '' || this.searchString === undefined){
      return;
    } else {
      this.isSearched = true;
      return this.service.searchBooksDB(this.searchString).subscribe(
        (data) => {
          this.books = data;
        },
        (error) => {
          console.log(error);
        }
      )
    }
  }

  public setSearchTitle() {
    this.searchByAuthor = false;
    this.searchByTitle = true;
  }

  public setSearchAuthor() {
    this.searchByAuthor = true;
    this.searchByTitle = false;
  }

  public previewBook(id: number) {
    this.router.navigate(['preview-books-db/' + id]);
  }

  public getBookImage(i: number) {
    return this.books[i].coverImage;
  }

}
