import { Component, OnInit, Output, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../../services/book.service';
import { Book } from '../../model/book';
import { Comment } from '../../model/comment';
import { CommentService } from '../../services/comment.service';
import { LoginService } from '../../services/login.service';
import { ScrollToConfigOptions, ScrollToService } from '@nicky-lenaers/ngx-scroll-to';


@Component({
  selector: 'app-preview-read',
  templateUrl: './preview-read.component.html',
  styleUrls: ['./preview-read.component.css']
})
export class PreviewReadComponent implements OnInit {

  public state: number;
  private sub: any;
  private id: number;
  private linkedCommentId: number;
  public book: Book;
  public currUserIsOwner: boolean;
  public private: boolean;
  public hover: boolean;
  public commentList: Comment[] = [];
  public bookId: number;
  public shareBookurl = "";
  public shareUrl = "";

  @Output("newComment") public newComment: Comment;

  constructor(private route: ActivatedRoute, private commentService: CommentService, private router: Router, private bookService: BookService, private loginService: LoginService, private scrollToService: ScrollToService) { }

  ngOnInit() {
    this.getParamFromUrl();
    
  }

  setUrl(){
    this.shareUrl = 'localhost:4200/#/share-book/'+this.bookId;
  }

  deleteBook() {

    this.bookService.deleteBook(this.id, this.loginService.user.id).subscribe(
      (data) => {
        let i = this.state;
        this.router.navigate(['book-list/' + i]);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * This method will retrieve id of the book from url.
   */
  public getParamFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.bookId = this.id;
      this.setUrl();
      this.getBookByID(this.id);
    });
  }

  public getCommentIdFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.linkedCommentId = +params['commId'];
      if (this.linkedCommentId !== undefined && this.linkedCommentId !== null) {
        this.goToComment();
      }
    });
  }

  /**
   * This method will retreive book by id from DB.
   * @param id - id of the book to be retrieved.
   */
  public getBookByID(id: number) {
    this.bookService.getOneWithUserId(id, this.loginService.user.id).subscribe(
      (data) => {
        this.book = data;
        this.getComments();
        this.checkState();
      },
      (error) => console.log(error)
    );
  }

  public getComments() {
    return this.commentService.getComments(this.id).subscribe(
      (data) => {
        this.commentList = data;
        this.getCommentIdFromUrl();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * This method will move a book to user's read list.
   */
  public moveToReadList() {
    return this.bookService.moveBookToReadList(this.book).subscribe(
      (data) => {
        this.book = data;
        this.router.navigate(['book-list/' + 1]);
      },
      (error) => console.log(error)
    );
  }

  /**
   * This method will move book to user's reading list.
   */
  public moveToReadingList() {
    return this.bookService.moveBookToReadingList(this.book).subscribe(
      (data) => {
        this.book = data;
        this.router.navigate(['book-list/' + 2]);
      },
      (error) => console.log(error)
    );
  }

  /**
   * This method will move book to user's to read list.
   */
  public moveToToReadList() {
    return this.bookService.moveBookToToReadList(this.book).subscribe(
      (data) => {
        this.book = data;
        this.router.navigate(['book-list/' + 3]);
      },
      (error) => console.log(error)
    );
  }

  public checkState() {
    if (this.book.state === 'READ') {
      this.state = 1;
    } else if (this.book.state === 'READING') {
      this.state = 2;
    } else if (this.book.state === 'TOREAD') {
      this.state = 3;
    }
  }


  setRating(rating: string) {
    if (rating === '5') {
      this.book.userRating = 5;
    } else if (rating === '4') {
      this.book.userRating = 4;
    } else if (rating === '3') {
      this.book.userRating = 3;
    } else if (rating === '2') {
      this.book.userRating = 2;
    } else if (rating === '1') {
      this.book.userRating = 1;
    } else if (rating === '0') {
      this.book.userRating = null;
    }
    this.bookService.updateBookStatus(this.book).subscribe(
      (error) => console.log(error)
    );
  }

  public getBookUrl() {
    return this.shareBookurl + this.bookId;
  }

  goToComment() {
    
    setTimeout(() => {
      const config: ScrollToConfigOptions = {   
        target: this.linkedCommentId + 'ID'
      };
      this.scrollToService.scrollTo(config);
    },
     900)
  }

  setPrivate() {
    this.book.bookIsPrivate = !this.book.bookIsPrivate;
    this.bookService.updateBookStatus(this.book).subscribe(
      () => {
      },
      (error) => console.log(error)
    );
  }

  getShareUrl() {
    return this.loginService.url + 'share-book/' + this.id;
  }

  someMethod(event) {
   this.router.navigate([this.router.url]);
   this.ngOnInit();
  }

}
