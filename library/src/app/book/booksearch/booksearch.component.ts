import { Component, OnInit } from '@angular/core';
import { BookApiService } from '../../services/book-api.service';
import { ApiBookList } from '../../model/api-book-list';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-booksearch',
  templateUrl: './booksearch.component.html',
  styleUrls: ['./booksearch.component.css']
})

export class BooksearchComponent implements OnInit {

  public sub: any;
  public searchString: string;
  public books: ApiBookList;

  constructor(private bookAPI: BookApiService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getParamFromUrl();
  }

  /**
   * This method will search book on the google with google's book api.
   */
  searchBooks() {
    return this.bookAPI.searchBooksByName(this.searchString).subscribe((data) => {
      this.books = data;
      this.spliceArrayIfNeeded(15);
    },
      (error) => {
        console.log(error);
      }
    )
  }

  public getParamFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.searchString = params['text'];
      if (this.searchString !== undefined && this.searchString !== '')
        this.searchString = atob(this.searchString);
      if (this.searchString !== undefined) {
        this.searchBooks();
      }
    });
  }

  /**
   * This method will splice an array if needed.
   * @param i - where should array be spliced.
   */
  spliceArrayIfNeeded(i: number) {
    if (this.books.items.length > i) {
      this.books.items.splice(i);
    }
  }

  previewBook(link: string) {
    link = btoa(link);
    this.router.navigate(['/book-preview/' + link]);
  }

  /**
   * This method will find a preview image for the book in the data which we got from api.
   * @param i - item in array which has to be searched for picture.
   */
  public getBookImage(i: number) {
    if (this.books.items[i].volumeInfo.imageLinks !== null && this.books.items[i].volumeInfo.imageLinks !== undefined) {
      if (this.books.items[i].volumeInfo.imageLinks.smallThumbnail !== null) {
        return this.books.items[i].volumeInfo.imageLinks.smallThumbnail;
      } else if (this.books.items[i].volumeInfo.imageLinks.thumbnail !== null && this.books.items[i].volumeInfo.imageLinks.thumbnail !== undefined) {
        return this.books.items[i].volumeInfo.imageLinks.thumbnail;
      } else {
        return "https://islandpress.org/sites/default/files/400px%20x%20600px-r01BookNotPictured.jpg";
      }
    } else {
      return "https://islandpress.org/sites/default/files/400px%20x%20600px-r01BookNotPictured.jpg";
    }
  }

}