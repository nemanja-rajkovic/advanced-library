import { Component, OnInit } from '@angular/core';
import { BookService } from '../../services/book.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../../model/book';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  public book: Book;
  id: number;
  private sub: any;

  constructor(private bookService: BookService, private route: ActivatedRoute, private router: Router, private loginService : LoginService) { }

  ngOnInit() {
    this.book = new Book();
    this.getParamFromUrl();
    this.getOne(this.id);
  }

  /**
   *  This method will get one Book by id from BookService. 
   * @param id - id of the book to be retrieved.
   */
  getOne(id: number) {
    this.bookService.getOneWithUserId(id, this.loginService.user.id).subscribe(
      (data) => {
        this.book = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * This method will retrieve parameter from url such as ID and convert it to number that we need
   * to retrieve the book we want.
   */
  public getParamFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
  }

}