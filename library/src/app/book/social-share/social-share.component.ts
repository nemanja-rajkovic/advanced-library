import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../../services/book.service';
import { Book } from '../../model/book';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-social-share',
  templateUrl: './social-share.component.html',
  styleUrls: ['./social-share.component.css']
})
export class SocialShareComponent implements OnInit {

  private sub: any;
  private id: number;
  public book: Book;
  public bookId: number;

  constructor(private route: ActivatedRoute, private router: Router, private bookService: BookService, private loginService: LoginService) { }

  ngOnInit() {
    this.getParamFromUrl();
  }

  /**
   * This method will retrieve id of the book from url.
   */
  public getParamFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.bookId = this.id;
      this.getBookByID(this.id);
    });
  }

  /**
   * This method will retreive book by id from DB.
   * @param id - id of the book to be retrieved.
   */
  public getBookByID(id: number) {
    this.bookService.getBookForSocialShare(id).subscribe(
      (data) => {
        this.book = data;
      },
      (error) => console.log(error)
    )
  }

}