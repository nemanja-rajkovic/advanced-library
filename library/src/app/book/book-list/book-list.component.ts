import { Component, OnInit } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Book } from '../../model/book';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  public read: boolean;
  public toRead: boolean;
  public reading: boolean;
  public readBooks: Book[] = [];
  public toReadBooks: Book[] = [];
  public readingBooks: Book[] = [];
  public id: number;
  public sortBy: string;
  public type: string;
  public nameSort: string;
  public nameType: string;
  public value: string;
  public criteria: string;
  private sub: any;
  private ownerIsVisiting = true;
  public selectedBook: Book;
  public hover = false;
  public state = -1;

  constructor(private bookService: BookService, private loginService: LoginService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.id = this.loginService.user.id;
    this.startComponent();
  }


  public startComponent() {
    this.setProp();
    this.getIdFromUrl();
    this.getStateFromUrl();
  }
  /**
    * This method will retrieve id of the book from url.
    */
  public getIdFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      if (params['id'] !== null && params['id'] !== undefined && params['id'] !== '') {
        this.id = +params['id'];
        this.ownerIsVisiting = false;
      }
    });
  }

  public getStateFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.state = +params['state'];
      if (this.state === 1) {
        this.showReadBooks();
      } else if (this.state === 2) {
        this.showReadingBooks();
      } else {
        this.showToReadBooks();
      }
    });
  }

  showReadBooks() {
    this.getReadList(this.id);
    this.setProp();
    this.read = true;
    this.toRead = false;
    this.reading = false;
  }

  showToReadBooks() {
    this.getToReadList(this.id);
    this.setProp();
    this.read = false;
    this.toRead = true;
    this.reading = false;
  }

  showReadingBooks() {
    this.getReadingList(this.id);
    this.setProp();
    this.read = false;
    this.toRead = false;
    this.reading = true;
  }

  setProp() {
    this.sortBy = 'title';
    this.type = 'asc';
    this.nameSort = 'Title';
    this.nameType = 'Ascending';
  }

  /**
   * This method will retrieve a list of books which user has already read.
   * @param id - id of the user.
   */
  public getReadList(id: number) {
    return this.bookService.getReadBooks(id).subscribe(
      (data) => {
        this.readBooks = data;
        if (this.ownerIsVisiting === false) {
          for (let i = 0; i < this.readBooks.length; i++) {
            if (this.readBooks[i].bookIsPrivate === true) {
              this.readBooks.splice(i, 1);
              i--;
            }
          }
        }
      },
      (error) => console.log(error)
    );
  }

  /**
  * This method will retrieve a list of books which user will read in future.
  * @param id - id of the user.
  */
  public getToReadList(id: number) {
    return this.bookService.getToReadBooks(id).subscribe(
      (data) => {
        this.toReadBooks = data;
        if (this.ownerIsVisiting === false) {
          for (let i = 0; i < this.toReadBooks.length; i++) {
            if (this.toReadBooks[i].bookIsPrivate === true) {
              this.toReadBooks.splice(i, 1);
              i--;
            }
          }
        }
      },
      (error) => console.log(error)
    );
  }

  /**
  * This method will retrieve a list of books which user is reading.
  * @param id - id of the user.
  */
  public getReadingList(id: number) {
    return this.bookService.getReadingBooks(id).subscribe(
      (data) => {
        this.readingBooks = data;
        if (this.ownerIsVisiting === false) {
          for (let i = 0; i < this.readingBooks.length; i++) {
            if (this.readingBooks[i].bookIsPrivate === true) {
              this.readingBooks.splice(i, 1);
              i--;
            }
          }
        }
      },
      (error) => console.log(error)
    );
  }

  public previewBook(id: number) {
    if (this.ownerIsVisiting === false) {
      this.router.navigate(['preview-books-db/' + id]);
    } else {
      this.router.navigate(['preview-book/' + id]);
    }
  }


  toggleEditable(event) {

    for (let i = 0, j = 0; i < this.readingBooks.length; i++) {
      if (event.target.checked) {
        this.criteria = this.readingBooks[i].author.name;
      }
    }
    console.log(this.criteria);
  }
}
