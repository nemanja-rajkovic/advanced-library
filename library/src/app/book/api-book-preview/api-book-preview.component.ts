import { Component, OnInit } from '@angular/core';
import { BookApiService } from '../../services/book-api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiBookModel } from '../../model/api-book-model';
import { Book } from '../../model/book';
import { UserRating } from '../../model/user-rating';
import { Author } from '../../model/author';
import { BookService } from '../../services/book.service';
import { Genre } from '../../model/genre';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-api-book-preview',
  templateUrl: './api-book-preview.component.html',
  styleUrls: ['./api-book-preview.component.css']
})
export class ApiBookPreviewComponent implements OnInit {

  private sub: any;
  public private: boolean;
  public book: ApiBookModel;
  public dbBook: Book;
  public state = 'READING';
  constructor(private service: BookApiService, private route: ActivatedRoute, private router: Router, private bookService: BookService, private loginService: LoginService) { }

  ngOnInit() {
    this.getParamFromUrl();
  }

  /**
     * This method will retrieve url which we need to retrieve the book we want.
   */
  public getParamFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.retrieveBookByUrl(atob(params['id']));
    });
  }

  /**
   * This method will retrieve the book from the google api.
   * @param link - link of the book to be aquired.
   */
  public retrieveBookByUrl(link: string) {
    return this.service.getBookByUrl(link).subscribe(
      (data) => {
        this.book = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  /**
   * This method will save the book from the google's book-api.
   */
  public saveBook(state: string) {
    this.dbBook = this.copyObject(this.book);
    this.dbBook.state = state;
      if (this.dbBook.description.length > 1998) {
        this.dbBook.description = this.dbBook.description.substring(0, 1998);
      }
    this.bookService.postBook(this.dbBook).subscribe(
      (data) => {
        if (state === 'READING') {
          this.readingBooks();
        } else if (state === 'READ') {
          this.readBooks();
        } else {
          this.toReadBooks();
        }
        this.dbBook = data;
      },
      (error) => console.log('Error: ' + error)
    );
  }

  /**
   * This method will copy properties of google api book object into our book object.
   * @param book - book to be copied.
   */
  public copyObject(book: ApiBookModel) {
    const genre: Genre[] = [];
    if (book.volumeInfo.categories != null) {
      for (let i = 0; i < book.volumeInfo.categories.length; i++) {
        genre[i] = new Genre(book.volumeInfo.categories[i]);
      }
    } else {
      genre[0] = new Genre('unknown');
    }

    let newBook = new Book(book.volumeInfo.title, (book.volumeInfo.industryIdentifiers[0].type === 'ISBN_13' ? book.volumeInfo.industryIdentifiers[0].identifier : book.volumeInfo.industryIdentifiers[1].identifier),
      book.volumeInfo.description, book.selfLink, (book.volumeInfo.imageLinks === undefined) ? undefined : (book.volumeInfo.imageLinks.smallThumbnail == null ? book.volumeInfo.imageLinks.thumbnail : book.volumeInfo.imageLinks.smallThumbnail),
      new UserRating(book.volumeInfo.ratingsCount, book.volumeInfo.averageRating), book.volumeInfo.pageCount, new Date(book.volumeInfo.publishedDate), book.volumeInfo.language,
      genre, book.volumeInfo.maturityRating, book.volumeInfo.publisher, new Author(book.volumeInfo.authors[0]), this.loginService.getAuthUser().id, this.private);
    if (newBook.coverImage === undefined) {
      newBook.coverImage = 'https://islandpress.org/sites/default/files/400px%20x%20600px-r01BookNotPictured.jpg';
    }
    return newBook;
  }

  toReadBooks() {
    this.router.navigate(['book-list/' + 3]);
  }

  readBooks() {
    this.router.navigate(['book-list/' + 1]);
  }

  readingBooks() {
    this.router.navigate(['book-list/' + 2]);
  }
}
