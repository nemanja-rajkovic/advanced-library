
export class UserRating {

    public numberOfVotes: number;
    public votes: number;

    public constructor(numberOfVotes?: number, votes?: number) {
        this.numberOfVotes = numberOfVotes;
        this.votes = votes;
    }
}
