export class Comment {

    public text: string;

    public postTime: Date;

    public id : number;

    public userId : number;

    public userName : string;

    public textToReply : string;
}