import { ApiBookModel } from "./api-book-model";

export class ApiBookList {
    kind: string;
    totalItems: number;
    items: ApiBookModel[] = [];
}
