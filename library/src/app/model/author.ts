import { Book } from "./book";

export class Author {
    public name: string;
    public bookList: Book[] = [];

    public constructor(name: string) {
        this.name = name;
    }
}