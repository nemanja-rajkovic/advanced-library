export class ApiBookModel {

  kind: string;
  selfLink: string;
  volumeInfo: {
    authors: string[];
    averageRating: number,
    categories: string[],
    description: string,
    imageLinks: {
      smallThumbnail: string,
      thumbnail: string
    },
    industryIdentifiers: [{
      identifier: string,
      type: string
    }],
    language: string,
    maturityRating: string,
    pageCount: number,
    publishedDate: string,
    publisher: string,
    ratingsCount: number,
    title: string
  }

}
