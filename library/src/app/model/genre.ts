
export class Genre {

    public genre: string;

    public constructor(genre: string) {
        this.genre = genre;
    }
}