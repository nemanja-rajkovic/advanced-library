export class CommentReport {

    public commentId: number;
    public commentOwnerId: number;
    public reporterId: number;
    public reportDescription: string;
    public bookId: number;

    public constructor(commId: number, commOwnId: number, reporterId: number, bookId: number, reportDesc?: string) {
        this.commentId = commId;
        this.commentOwnerId = commOwnId;
        this.reporterId = reporterId;
        this.reportDescription = reportDesc;
        this.bookId = bookId;
    }
}
