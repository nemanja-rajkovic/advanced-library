
export class BookComment {

    public commentId : number;
    public text : string;
    public postTime : Date;
    public userName : string;
    public bookId : number;
    public commentUserId : number;
    public textToReply : string;
}
