export class CommentReply {
    
    public commentId : number;
    public text : string;
    public postTime : Date;
    public userName : string;
    public bookId : number;
    public textToReply : string;
    public commentUserId : number;
    public commentOwnerId : number;
}
