export class LibraryUser {
    
    public id: number;
    public username: string;
    public email: string;
    public password: string;
    public name: string;
    public gender : boolean;
    public description : string;
    public pictureUrl : any;
    public blocked : boolean;
    public blockedUntil : Date;
   
    public dateJoined : Date;

    public constructor(id?: number) {
        this.id = id;
    }
}
