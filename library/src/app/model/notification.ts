export class Notification {
  
    public id : number;
    public text : string;
    public bookId : number;
    public commentId : number;
    public notificationIsSeen : boolean;
}
