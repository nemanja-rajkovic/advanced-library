
import { UserRating } from "./user-rating";
import { Author } from "./author";
import { Genre } from "./genre";
import { LibraryUser } from "./library-user.model";
import { Comment } from "./comment";

export class Book {

    public title: string;

    public userId : number;

    public isbn13: string;

    public libraryUser: LibraryUser;

    public description: string;

    public selfLink: string;

    public state : string;

    public coverImage: string;

    public globalRating: UserRating;

    public userRating: number;

    public pageCount: number;

    public publishedDate: Date;

    public language: string;

    public genreList: Genre[] = [];

    public maturityRating: string;

    public comments: Comment[] = [];

    public publisher: string;

    public bookIsPrivate: boolean;

    public author: Author;

    public hover : boolean;

    public constructor(title?: string, isbn13?: string, desc?: string, selfLink?: string, cover?: string, globalRating?: UserRating, pageCount?: number, publishedDate?: Date, language?: string, genreList?: Genre[],
        maturityRating?: string, publisher?: string, author?: Author, userId?: number, bookIsPrivate?: boolean) {
        this.title = title; this.isbn13 = isbn13; this.description = desc; this.selfLink = selfLink; this.coverImage = cover; this.globalRating = globalRating; this.pageCount = pageCount;
        this.publishedDate = publishedDate; this.language = language; this.genreList = genreList; this.maturityRating = maturityRating; this.publisher = publisher; this.author = author;
        this.userId = userId; this.bookIsPrivate = bookIsPrivate;

    }
}