import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './templates/registration/registration.component';
import { CommonModule } from '@angular/common';
import { LogInComponent } from './templates/log-in/log-in.component';
import { HomeComponent } from './templates/home/home.component';
import { BookComponent } from './book/single-book/book.component';
import { BooksearchComponent } from './book/booksearch/booksearch.component';
import { ApiBookPreviewComponent } from './book/api-book-preview/api-book-preview.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';
import { BookListComponent } from './book/book-list/book-list.component';
import { PreviewReadComponent } from './book/preview-read/preview-read.component';
import { SearchBooksFromDbComponent } from './book/search-books-from-db/search-books-from-db.component';
import { PreviewDbBooksComponent } from './book/preview-db-books/preview-db-books.component';
import { ProfileComponent } from './profile/profile.component';
import { SearchProfileComponent } from './profile/search-profile/search-profile.component';
import { VisitProfileComponent } from './profile/visit-profile/visit-profile.component';
import { SocialShareComponent } from './book/social-share/social-share.component';
import { AdminPageComponent } from './profile/admin-page/admin-page.component';
import { LogoutComponent } from './templates/logout/logout.component';
import { ReportedCommentComponent } from './comments/reported-comment/reported-comment.component';


const routes: Routes = [

  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'registration', component: RegistrationComponent },
  { path: 'login', component: LogInComponent },
  { path: 'login/:id/:commId', component: LogInComponent },
  { path: 'book/:id', component: BookComponent, canActivate: [AuthGuard] },
  { path: 'search-books', component: BooksearchComponent, canActivate: [AuthGuard] },
  { path: 'search-books/:text', component: BooksearchComponent, canActivate: [AuthGuard] },
  { path: 'book-preview/:id', component: ApiBookPreviewComponent, canActivate: [AuthGuard] },
  { path: 'book-list/:state', component: BookListComponent, canActivate: [AuthGuard] },
  { path: 'book-list/:id/:state', component: BookListComponent, canActivate: [AuthGuard] },
  { path: 'preview-book/:id', component: PreviewReadComponent, canActivate: [AuthGuard] },
  { path: 'preview-book/:id/:commId', component: PreviewReadComponent, canActivate: [AuthGuard] },
  { path: 'search-books-db', component: SearchBooksFromDbComponent, canActivate: [AuthGuard] },
  { path: 'preview-books-db/:id', component: PreviewDbBooksComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'profile-search', component: SearchProfileComponent, canActivate: [AuthGuard] },
  { path: 'visit-profile/:id', component: VisitProfileComponent, canActivate: [AuthGuard] },
  { path: 'share-book/:id', component: SocialShareComponent },
  { path: 'admin-page', component: AdminPageComponent, canActivate: [AuthGuard] },
  { path: 'logout', component: LogoutComponent },
  { path: 'reported-comment/:commId/:bookId/:notId', component: ReportedCommentComponent, canActivate: [AuthGuard] }

];

@NgModule({
  imports: [
    CommonModule,
    [RouterModule.forRoot(routes, { useHash: true })]
  ],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule { }