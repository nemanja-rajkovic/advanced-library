import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { CommentService } from '../../services/comment.service';
import 'rxjs/add/observable/interval';
import { Observable } from 'rxjs';
import { Notification } from '../../model/notification';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public notifications: Notification[] = [];
  public searchText: string;
  public unread : boolean = false;

  constructor(private login: LoginService, private router: Router, private commentService: CommentService, private notifService: NotificationService) { }

  ngOnInit() {

    Observable.interval(3000)
      .subscribe(
        () => {
          if (this.login.user !== null && this.login.user !== undefined) {
            this.getNotifications();
          }
        }
      )
  }

  getNotifications() {
    return this.notifService.getNotifications(this.login.user.id).subscribe(
      (data) => {
        this.notifications = data;
        for(let i = 0; i<this.notifications.length; i++) {
          if(this.notifications[i].notificationIsSeen == false) {
            this.unread = true;
          } else {
            this.unread = false;
          }
        }
      },
      (error) => {
        console.log(error);
      }
    )
  }

  public profileNavigate() {
    if (this.login.user != null) {
      this.router.navigate(['/profile/' + this.login.user.id]);
    }
  }

  seeNotification(i: number) {
    //reported-comment/:commId/:bookId
    if (this.login.hasRoleAdmin()) {
      this.notifService.updateNotification(this.login.user.id, this.notifications[i].id).subscribe(
        (data) => {
          this.router.navigate(['reported-comment/' + this.notifications[i].commentId + "/" + this.notifications[i].bookId + "/" + this.notifications[i].id]);
        },
        (error) => console.log(error)
      )
    } else {
      this.notifService.updateNotification(this.login.user.id, this.notifications[i].id).subscribe(
        (data) => {
          this.router.navigate(['preview-book/' + this.notifications[i].bookId + "/" + this.notifications[i].commentId]);
        },
        (error) => console.log(error)
      )
    }
  }

  deleteNotification(i: number) {
    let notId = this.notifications[i].id;
    this.notifications.splice(i, 1);
    this.notifService.deleteNotification(this.login.user.id, notId).subscribe(
      (data) => { },
      (error) => {
        console.log(error);
      }
    )
  }

  searchApi() {
    let t = btoa(this.searchText);
    this.searchText = "";
    this.router.navigate(['search-books/' + t]);
  }

}
