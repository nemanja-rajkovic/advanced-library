import { Component, OnInit } from '@angular/core';
import { BookApiService } from '../../services/book-api.service';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public books: BestBooks[] = [];

  public secondBooks: BestBooks[] = [];
  public genre = 'Thriller';
  constructor(private api: BookApiService, private router: Router, private loginService: LoginService) { }

  ngOnInit() {

    this.loadBooks();
    const num = this.getRandomInt(1, 6);
    if (num === 1) {
      this.loadClassicBooks();
    } else if (num === 2) {
      this.loadDramaBooks();
    } else if (num === 3) {
      this.loadFictionBooks();
    } else if (num === 4) {
      this.loadHorrorBooks();
    } else if (num === 5) {
      this.loadScifiBooks();
    } else {
      this.loadThrillerBooks();
    }

  }

  getBookByUrl(url: string, i: number) {
    return this.api.getBookByUrl(url).subscribe(
      (data) => {
        this.books[i] = data;
      },
      (error) => console.log(error)
    );
  }

  navigate(i: number) {
    this.router.navigate(['/book-preview/' + btoa(this.books[i].link)]);
  }

  navigateSecondBooks(i: number) {
    this.router.navigate(['/book-preview/' + btoa(this.secondBooks[i].link)]);
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  loadScifiBooks() {
    this.genre = 'Sci-fi';
    this.secondBooks[0] = new BestBooks();
    this.secondBooks[0].link = 'https://www.googleapis.com/books/v1/volumes/OYtkbGl2j0sC';
    this.secondBooks[0].title = 'Fahrenheit 451';
    this.secondBooks[0].picUrl = 'http://books.google.com/books/content?id=OYtkbGl2j0sC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71OfCGlcgwwT6ujGtDxAjTJPz-4ldbPKmz-rSchiQxcryj-0YQZtFMYfDN9xbtwT1sAusq0-dKwdKd9Hzq-lpCjBLOuBStmyOWcJLqXZIC9p5vv1lZDhoyert-ivgOloxN1YwU0&source=gbs_api';
    this.secondBooks[0].author = 'Ray Bradbury';

    this.secondBooks[1] = new BestBooks();
    this.secondBooks[1].link = 'https://www.googleapis.com/books/v1/volumes/wl8tAgAAQBAJ';
    this.secondBooks[1].title = 'Dina\'s Book';
    this.secondBooks[1].picUrl = 'http://books.google.com/books/content?id=wl8tAgAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71vx9AENZSXjeRFzg7vffwuPgrKdcWLAYN_Bi8Ai244_RP-VM-rvAeta9xcQwBT_acxiCuOwFdkFYQ7pn--O1xfXJkKC56zzntD_QiPdq1n_zbqESOgBusAXkHhy5NWhHCxL810&source=gbs_api';
    this.secondBooks[1].author = 'Herbjorg Wassmo';

    this.secondBooks[2] = new BestBooks();
    this.secondBooks[2].link = 'https://www.googleapis.com/books/v1/volumes/YGouDwAAQBAJ';
    this.secondBooks[2].title = 'The Forever War #1';
    this.secondBooks[2].picUrl = 'http://books.google.com/books/content?id=YGouDwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE70Rl-jZDsmgNzXcFQ_8xq2izbhyNcp2O2QSWTNpGxByYoFiK10pZBobYq7grJRHxUs872WguATr38Dn0-zCdouDGfx3w7XoQK_8mT803Cp8-YNuF7Tg7jlrsZoA9ZN4Ozea1h_6&source=gbs_api';
    this.secondBooks[2].author = 'Joe Haldeman';

    this.secondBooks[3] = new BestBooks();
    this.secondBooks[3].link = 'https://www.googleapis.com/books/v1/volumes/lKt5laoj1coC';
    this.secondBooks[3].title = 'The Mote in God\'s Eye';
    this.secondBooks[3].picUrl = 'http://books.google.com/books/content?id=lKt5laoj1coC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71EUPFj4yigyAx2EtnfPPg8FJMu8qmIAyZAninyOOqbToM9rvn0ErdAlmMKz6a-OMapo0YMlN0i7cTKq9o_cz2icshLAC55vm5TLsevh3AGx_6ibymPa7cOpJOF7t8gjAGS4Nck&source=gbs_api';
    this.secondBooks[3].author = 'Larry Niven';

    this.secondBooks[4] = new BestBooks();
    this.secondBooks[4].link = 'https://www.googleapis.com/books/v1/volumes/y5F8tgAACAAJ';
    this.secondBooks[4].title = 'The Call of Cthulhu';
    this.secondBooks[4].picUrl = 'http://books.google.com/books/content?id=y5F8tgAACAAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE70S5hyv7zP-VVfTlsn1Me-Uorn8IX-jAvQ8U2DjxAiPSZ6eDj2FKmz2Lhs_SVPnKQR0iSWVXDOBdMXlGFN6kSt6lHD3l2CyrJ-4IwyTRZoZue7bkaAVmO7iKeR65s-8SOaJUeUi&source=gbs_api';
    this.secondBooks[4].author = 'H. P. Lovecraft';


  }

  loadDramaBooks() {
    this.genre = 'Drama';
    this.secondBooks[0] = new BestBooks();
    this.secondBooks[0].link = 'https://www.googleapis.com/books/v1/volumes/j5ImxqeXja8C';
    this.secondBooks[0].title = 'Emma';
    this.secondBooks[0].picUrl = 'http://books.google.com/books/content?id=j5ImxqeXja8C&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE70QWLoj7itAjKraU5VnfMydcqmH6KPc1ZdaNB-0ZNLGWRM780Tw_0ns7Qx97Oph04p7o0LEqf56zCudVQqUSWM-Y6sDVuhRh1iOJjIMraovnC66CrbDKP2aofIpJgn44FbNCHt_&source=gbs_api';
    this.secondBooks[0].author = 'Jane Austen';

    this.secondBooks[1] = new BestBooks();
    this.secondBooks[1].link = 'https://www.googleapis.com/books/v1/volumes/qMgWBQAAQBAJ';
    this.secondBooks[1].title = 'Gone Girl';
    this.secondBooks[1].picUrl = 'http://books.google.com/books/content?id=qMgWBQAAQBAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE72B5w5OYRabhie8NIKk9mjIl2JuY9tz7M7VkpnVAzJ6Oq7NdpC3DLGk35wEGVN8GQEy1Nc3pvBHsCKBevCW8lXTKXmWP3XCWgzC9k-BmUdlCcgy7FpZW2ndblsJdx_UUlNoem73&source=gbs_api';
    this.secondBooks[1].author = 'Gillian Flynn';

    this.secondBooks[2] = new BestBooks();
    this.secondBooks[2].link = 'https://www.googleapis.com/books/v1/volumes/webh0KkT9o8C';
    this.secondBooks[2].title = 'Hamlet';
    this.secondBooks[2].picUrl = 'http://books.google.com/books/content?id=webh0KkT9o8C&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE73k3hNj_n-Ox0wyNMez5ppCaL4ZuI7JzREsC8piDRqwjR3faVTpSPVc1cplgzPxm84VWO0YgwTDPaSqVFr2zdshrCv7BZGMvApHJM7GJIcu0-D4SEvCXiKyIK28XC4tzTOfBpOd&source=gbs_api';
    this.secondBooks[2].author = 'William Shakespeare';

    this.secondBooks[3] = new BestBooks();
    this.secondBooks[3].link = 'https://www.googleapis.com/books/v1/volumes/zQ-Ar3ixRR8C';
    this.secondBooks[3].title = 'Crime and Punishment';
    this.secondBooks[3].picUrl = 'http://books.google.com/books/content?id=zQ-Ar3ixRR8C&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE72FibXXyvzI1qONDmvqeoh_IRVGUZf5x1PUlQ_Xc_KVNtdwwvlQVaz0Q0CQNlgzWhdcF6Xjs7HQnDi-qE2gAU0H4uc-6an-NU4qC8A4Hyre1IH0uxivgAcyMVt095UsZ5cRv_4y&source=gbs_api';
    this.secondBooks[3].author = 'Fyodor Dostoyevsky';

    this.secondBooks[4] = new BestBooks();
    this.secondBooks[4].link = 'https://www.googleapis.com/books/v1/volumes/tFgHrgEACAAJ';
    this.secondBooks[4].title = 'To Kill a Mockingbird';
    this.secondBooks[4].picUrl = 'http://books.google.com/books/content?id=tFgHrgEACAAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE70u7gygRTn3v2aA4j2Y3iUHYF70heJ96Crpk1i0LdtZxBArs3d_lPVJsOlZrn2dQOeIhA2EUt4QGJgXhS_EXtbb7U5PMMA321cH9sqUV87Rfvhun7baVpupHPh_uVDx5v1HswPV&source=gbs_api';
    this.secondBooks[4].author = 'Harper Lee';


  }

  loadFictionBooks() {
    this.genre = 'Fiction';
    this.secondBooks[0] = new BestBooks();
    this.secondBooks[0].link = 'https://www.googleapis.com/books/v1/volumes/PgtvswEACAAJ';
    this.secondBooks[0].title = 'A Song of Ice and Fire';
    this.secondBooks[0].picUrl = 'http://books.google.com/books/content?id=PgtvswEACAAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE70gaGJN3oQk7pLo34xshBKG04G8aYQCvqeMbdnailsMZHUpNFPW6huXVLDU_-wpMHWw1gGEffEaBNOsemza_yl8fWKpRdVkw8ttRyqSlTx-Y4ASNAlfBBQS6fvcJ3g0ZX9SPpQo&source=gbs_api';
    this.secondBooks[0].author = 'George R. R. Martin';

    this.secondBooks[1] = new BestBooks();
    this.secondBooks[1].link = 'https://www.googleapis.com/books/v1/volumes/K8BvCQAAQBAJ';
    this.secondBooks[1].title = 'Brave New World';
    this.secondBooks[1].picUrl = 'http://books.google.com/books/content?id=K8BvCQAAQBAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE73NbkMJPtaXesq8Wm6zLCjty42UCPbJ4H_sed7LZUJuwwQhSBrInyupK6BMdKN1HagwAkUW7jubPVVrp0Q_w40562p4oQf0eEYB0UmqHvbtvNAhRhIAQnFfQcN4RPMdTuvM4OBr&source=gbs_api';
    this.secondBooks[1].author = 'Aldous Huxley';

    this.secondBooks[2] = new BestBooks();
    this.secondBooks[2].link = 'https://www.googleapis.com/books/v1/volumes/yl4dILkcqm4C';
    this.secondBooks[2].title = 'The Lord of the Rings';
    this.secondBooks[2].picUrl = 'http://books.google.com/books/content?id=yl4dILkcqm4C&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71D2230MEpdCn9tq9V6J221qO4AdomwNtO_ZYwHZ5FOjZdGnwvjaV1QfQ-qEkjuqLEa15FhtDtajIvCvs1XVU5q9A-Hg9h1_JpVI2WGUeJqZS--V2lD7qgh96wyr-Kjao3EeR1F&source=gbs_api';
    this.secondBooks[2].author = 'J.R.R. Tolkien';

    this.secondBooks[3] = new BestBooks();
    this.secondBooks[3].link = 'https://www.googleapis.com/books/v1/volumes/3nAyDwAAQBAJ';
    this.secondBooks[3].title = 'Her Body & Other Parties';
    this.secondBooks[3].picUrl = 'http://books.google.com/books/content?id=3nAyDwAAQBAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE71uTHRcWQHafAmxKa4OjzyjMvSR2IAakTiwUpBSHO2f1Fe_IZrfDlAyGDTABqiP_abXM5cAveyh40VzfmdfQg1SjmlWhHayYFiyU_dhI-Dx3P3uwbrjot0bEN_V8VGLUwyRaQlY&source=gbs_api';
    this.secondBooks[3].author = 'Carmen Maria Machado';

    this.secondBooks[4] = new BestBooks();
    this.secondBooks[4].link = 'https://www.googleapis.com/books/v1/volumes/XVQZDAAAQBAJ';
    this.secondBooks[4].title = 'Nineteen Eighty-four';
    this.secondBooks[4].picUrl = 'http://books.google.com/books/content?id=XVQZDAAAQBAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE71fCoUY00vO6TNr0z27zrT2syGcU6iYdGeqO2CP-6HDwFlNe59GgAZ0CeMSaGbjDEKXIdRts96Q1IkJKqV5Yrx8bRBtaEsS3hnqrrJq2YH1AM4iUwfFz3EzDUFDQIYYsgHwAmwt&source=gbs_api';
    this.secondBooks[4].author = 'George Orwell';


  }

  loadHorrorBooks() {
    this.genre = 'Horror';
    this.secondBooks[0] = new BestBooks();
    this.secondBooks[0].link = 'https://www.googleapis.com/books/v1/volumes/vUZoxITGufkC';
    this.secondBooks[0].title = 'The Shining';
    this.secondBooks[0].picUrl = 'http://books.google.com/books/content?id=vUZoxITGufkC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE73sJREnq1PwAjpp_gV5KITooaQPgvN2HGN8WLLUbKYENtQVJD03kI_FK1rbjCtBApWUp0K8yrgWRszBEDRWGFYu4w-nAj6NJ8l3Rxecs5MzEm5tUCquDfEsbD8mTREsxhMOFlsN&source=gbs_api';
    this.secondBooks[0].author = 'Stephen King';

    this.secondBooks[1] = new BestBooks();
    this.secondBooks[1].link = 'https://www.googleapis.com/books/v1/volumes/1ZlbAgAAQBAJ';
    this.secondBooks[1].title = 'Frankenstein\'s monster';
    this.secondBooks[1].picUrl = 'http://books.google.com/books/content?id=1ZlbAgAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE73YwTkICo61mit_Adpnp6XxnMPDfilmIQgi5XsvmzLQa1la8-A6L3R7sxNd5PIfHGF3cSWftN5Q9hkiR3gaKVGSa7KI2oiWF5VJ0qmgfmgUtV8RPTiTrT-Hg6QWfjkFGCMk4K9T&source=gbs_api';
    this.secondBooks[1].author = 'Harold Bloom';

    this.secondBooks[2] = new BestBooks();
    this.secondBooks[2].link = 'https://www.googleapis.com/books/v1/volumes/U5_iCgAAQBAJ';
    this.secondBooks[2].title = 'House of Leaves';
    this.secondBooks[2].picUrl = 'http://books.google.com/books/content?id=U5_iCgAAQBAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE731f25ILXSzqOWzSRG9_jVB5nOjtv1h5wOtDbLi0urkKYByq8o8ykM0pPO5CVXinRoUlC9rrZ46R30PA_CmPlLhlF-gCVBcNd5r8zi4nL23Wj6TlEYblnhNwAm--emawMOAXdKl&source=gbs_api';
    this.secondBooks[2].author = 'Mark Z. Danielewski';

    this.secondBooks[3] = new BestBooks();
    this.secondBooks[3].link = 'https://www.googleapis.com/books/v1/volumes/fVWQDQAAQBAJ';
    this.secondBooks[3].title = 'IT';
    this.secondBooks[3].picUrl = 'http://books.google.com/books/content?id=fVWQDQAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE70rD6k6FSJEaDhne2C51VJC4UhPaqguRQkh8A_sezFi4qP34ai2W7KoGzgtlMYQRregLjvBAfDQkyv3fsilSMbTsmIp7PEA6RQ00GezS76Fs1kBAsIw4z7htGJlg6NoS7MPdeo_&source=gbs_api';
    this.secondBooks[3].author = 'Stephen King';

    this.secondBooks[4] = new BestBooks();
    this.secondBooks[4].link = 'https://www.googleapis.com/books/v1/volumes/ArNnwDGH1fQC';
    this.secondBooks[4].title = 'Dracula';
    this.secondBooks[4].picUrl = 'http://books.google.com/books/content?id=ArNnwDGH1fQC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE71xNfYmeiHOSLNGLHMcu64kF2W_hPm2o7-noaOt1Uohdb4yuwmnkCIRXwR_PnaTpdapojCOXgX74yGrrU4Nk31noq8ljRrna3sXAib4_eF-BwZepdOoED4KJLSgXAyPcOnpCZDa&source=gbs_api';
    this.secondBooks[4].author = 'Bram Stoker';

  }

  loadClassicBooks() {
    this.genre = 'Classic';
    this.secondBooks[0] = new BestBooks();
    this.secondBooks[0].link = 'https://www.googleapis.com/books/v1/volumes/GSqGAAAAQBAJ';
    this.secondBooks[0].title = 'The Divine Comedy';
    this.secondBooks[0].picUrl = 'http://books.google.com/books/content?id=GSqGAAAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE73jpGf33xmNkFDg-eIewQKWQkqcOjT52PaQKNQMXn3ShpsQBiGSzDy_mxzfs7EugTcoRpAr0r_7aCJ789ORCn2zV7cqcBmtRu9zaf07wZOnfUpiHjskvjQ1tfiu0u0pU11NBxHq&source=gbs_api';
    this.secondBooks[0].author = 'Dante Alighieri';

    this.secondBooks[1] = new BestBooks();
    this.secondBooks[1].link = 'https://www.googleapis.com/books/v1/volumes/qhQAywOYz10C';
    this.secondBooks[1].title = 'The Odyssey';
    this.secondBooks[1].picUrl = 'http://books.google.com/books/content?id=qhQAywOYz10C&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE72-D3HCvoeI5qpPWxk22AB5gTsthGzIyzC61gELGYRHK-oqSw76F2R2qd7nWY21suTZvVstJxiNj7uHvHjxzXjjPByb9xBpf_rTxp4qCwrHTlG__3dVSVH2_MFN6z8YUx3CRUKV&source=gbs_api';
    this.secondBooks[1].author = 'Homer';

    this.secondBooks[2] = new BestBooks();
    this.secondBooks[2].link = 'https://www.googleapis.com/books/v1/volumes/k21QvgAACAAJ';
    this.secondBooks[2].title = 'Ulysses';
    this.secondBooks[2].picUrl = 'http://books.google.com/books/content?id=k21QvgAACAAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE72aGnPWiKs5K6dKclZSSg6TAjN5u5vB81Vdi1H3GV5cAK671uN8Qzx0B2LNUSFpipMnHAn8ypNm4MlQhnFu4ruiNuziNN5fDmdggLuCmJF8Mm2AY7NsN5oiq6-WF5Nwu8znqC-Q&source=gbs_api';
    this.secondBooks[2].author = 'Joyce James';

    this.secondBooks[3] = new BestBooks();
    this.secondBooks[3].link = 'https://www.googleapis.com/books/v1/volumes/FICLzcp22b8C';
    this.secondBooks[3].title = 'Don Quixote';
    this.secondBooks[3].picUrl = 'http://books.google.com/books/content?id=FICLzcp22b8C&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE712CToUiq6Z-d6mo1XrHXUrXJtKFuREWa93ql34WYyYtSEg9rdOnh_72OC6vrOIrBZ1VjSvwwfB_chtwfnFzRfmxnU9BaxJ0EULvTUFhkZ_FhASdI29mD4Q8B69fLnsEDMvntpq&source=gbs_api';
    this.secondBooks[3].author = 'Miguel de Cervantes Saavedra';

    this.secondBooks[4] = new BestBooks();
    this.secondBooks[4].link = 'https://www.googleapis.com/books/v1/volumes/RBv3wE6l-RYC';
    this.secondBooks[4].title = 'In Search of Lost Time';
    this.secondBooks[4].picUrl = 'http://books.google.com/books/content?id=RBv3wE6l-RYC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE72Q0oSuzBGkp3EqhcF_gvfsyKWNg86NyGp1ajKsWtZjb13QBFVczlhpqIV07GVjidnkRypN9iBO5zv1-CLi1u8CTmA7xUqNdLV5Q3DVsqvg-jUBumOzA1g0gjevKvNGNyuQ_cqB&source=gbs_api';
    this.secondBooks[4].author = 'David Ellison';

  }

  loadThrillerBooks() {
    this.genre = 'Thriller';
    this.secondBooks[0] = new BestBooks();
    this.secondBooks[0].link = 'https://www.googleapis.com/books/v1/volumes/DLXmJRys8b0C';
    this.secondBooks[0].title = 'A Time to Kill';
    this.secondBooks[0].picUrl = 'http://books.google.com/books/content?id=DLXmJRys8b0C&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE71rZlpD3ya42O5es8sfBCAUK4lASVk0tTQUR-2fEaRDMidMRI9-FZIuSUPzXshoo3NZvGLYN7PNwcbRYw7e8phZ2q12Uyl_LJOnyEe4PqEyGkybHq_VLm8_CfriUlTbfUspvh3-&source=gbs_api';
    this.secondBooks[0].author = 'John Grisham';

    this.secondBooks[1] = new BestBooks();
    this.secondBooks[1].link = 'https://www.googleapis.com/books/v1/volumes/qQNnCswKF5EC';
    this.secondBooks[1].title = 'Before I Go To Sleep';
    this.secondBooks[1].picUrl = 'http://books.google.com/books/content?id=qQNnCswKF5EC&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE734fwHPi6N5uyEz1zIgrd0-nHARsa29VJri2_8hqArQ-cgIFrctFLiMWcmwLAAQ4CRXZHGnIGs9kzkr8EpTER7yMbUnIRDZmRS59lYpF595jPmdeMA_htmpKHS4Z8OCTBzoG1f1&source=gbs_api';
    this.secondBooks[1].author = 'S. J. Watson';

    this.secondBooks[2] = new BestBooks();
    this.secondBooks[2].link = 'https://www.googleapis.com/books/v1/volumes/ADiMDQAAQBAJ';
    this.secondBooks[2].title = 'Killing Floor';
    this.secondBooks[2].picUrl = 'http://books.google.com/books/content?id=ADiMDQAAQBAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE70VxP9fGmYulG5aRlSKLa69caugwthwUzp6qAImMddyvSDmLqPzIfZ-xwz_BhZtU3Ds20iOXgzZ-DLFiudjnnXZGG_mz-PXMn58J5xtQSFHSQGCpeVRIzlw0Qk5ZLVD-j5JO4yw&source=gbs_api';
    this.secondBooks[2].author = 'Lee Child';

    this.secondBooks[3] = new BestBooks();
    this.secondBooks[3].link = 'https://www.googleapis.com/books/v1/volumes/aFNaDwAAQBAJ';
    this.secondBooks[3].title = 'The Girl on the Train';
    this.secondBooks[3].picUrl = 'http://books.google.com/books/content?id=aFNaDwAAQBAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE73YnJtT2UxAy7p4CUJecnAsmMp5KeeFL1uzP-Cdzz-sps5kB-rW-Ed-RLYuu9Q1DLVU4WyGfvabxMWZt7z80jD4J1ARX3mwJbRmvek_9WLJXVC-rcCLFv1p817uXERPm_L5UtYc&source=gbs_api';
    this.secondBooks[3].author = 'Paula Hawkins';

    this.secondBooks[4] = new BestBooks();
    this.secondBooks[4].link = 'https://www.googleapis.com/books/v1/volumes/qMgWBQAAQBAJ';
    this.secondBooks[4].title = 'Gone Girl';
    this.secondBooks[4].picUrl = 'http://books.google.com/books/content?id=qMgWBQAAQBAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE73Wppx3ptLT2EyijFNMAGD7ex0317cqiueWja_ElxkjmaOy7WEd4zJR_kvUi6ZIaKZazGgS5prVW9Wy5UuZEbamNw7c5YBjOOUeE9z_HB88U3vqVvnTYpRIML55cPs1gqxsWjob&source=gbs_api';
    this.secondBooks[4].author = 'Gillian Flynn';

  }

  loadBooks() {
    this.books[0] = new BestBooks();
    this.books[0].link = 'https://www.googleapis.com/books/v1/volumes/kotPYEqx7kMC';
    this.books[0].title = '1984';
    this.books[0].picUrl = 'http://books.google.com/books/content?id=kotPYEqx7kMC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE73_8LHU7uVQw4N4j1r9bboiPI1zgAIpd1oNFXaAz7yOFaIuNShIngMBilNuMfSUVZGW8v9pWu9V_96XleagmieApr4jHlEfEfRRIWthO3Mlc7fcZb15DbV66nWPXoQ1qNLCEg0Z&source=gbs_api';
    this.books[0].author = 'George Orwell';

    this.books[1] = new BestBooks();
    this.books[1].link = 'https://www.googleapis.com/books/v1/volumes/5GbdTc9OJ78C';
    this.books[1].title = 'Pride and Prejudice';
    this.books[1].picUrl = 'http://books.google.com/books/content?id=5GbdTc9OJ78C&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE717waLylB8AWyjMV_ocLSbknnHMLxVfsLopr66x3BgtTjwkNqi55Yop50WjA_pQMEkAqa0_ke-6sQPDUqPyEx3dKw0uijrYRTj0sxAoWHT4QnuE4tZt8-D0NH7aR_uQvG8nFmvZ&source=gbs_api';
    this.books[1].author = 'Jane Austen';

    this.books[2] = new BestBooks();
    this.books[2].link = 'https://www.googleapis.com/books/v1/volumes/S_ZqjwEACAAJ';
    this.books[2].title = 'The Catcher in the Rye';
    this.books[2].picUrl = 'http://books.google.com/books/content?id=S_ZqjwEACAAJ&printsec=frontcover&img=1&zoom=5&imgtk=AFLRE70zKBabyqm2TN6gUzHyq1yCmuznCZyQHa1xXWVim_e1ZPNh4LLChu4jpSsqCpULsB0DyXVNgCF5mvt7Xuxp6APB5Qr8gbmnSWMqKALKXi0HKVl_F8ILq9QvOE-Vlig1puvKoa39&source=gbs_api';
    this.books[2].author = 'Jerome Salinger';

    this.books[3] = new BestBooks();
    this.books[3].link = 'https://www.googleapis.com/books/v1/volumes/moYNAAAAYAAJ';
    this.books[3].title = 'Romeo and Juliet';
    this.books[3].picUrl = 'http://books.google.com/books/content?id=moYNAAAAYAAJ&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE7347jqz9xAcfHQxQpFO78OxVhrbCLAXZ_iFKxnTC-J1GuxUP9VmbvzuH5U327Du8Xk6zw7JQUOSM5CRzXkmgQGKzosmHNbL9MZ2NdQPRgoYENSzGpzsp7xnSxmv8MPB4wlHQbp9&source=gbs_api';
    this.books[3].author = 'William Shakespeare';

    this.books[4] = new BestBooks();
    this.books[4].link = 'https://www.googleapis.com/books/v1/volumes/DKU1EsmnpMQC';
    this.books[4].title = 'Lord of the Flies';
    this.books[4].picUrl = 'http://books.google.com/books/content?id=DKU1EsmnpMQC&printsec=frontcover&img=1&zoom=5&edge=curl&imgtk=AFLRE70GmN5oSR7m6eu97zjJkGh3y3CyXCXDGGAXRgtcHBnW1Ct2o0RpuAnpGKPq_4sxGaVKuuCnFNJwt4MCyeAg8Ozc_D94kTPfeCGlGOQK-CYKdM3Q4CqsvYj_q8ZB61aVER68FlNx&source=gbs_api';
    this.books[4].author = 'William Golding';
  }

}

export class BestBooks {

  public link: string;
  public picUrl: any;
  public author: string;
  public title: string;
}
