import { Component, OnInit } from '@angular/core';
import { LibraryUser } from '../../model/library-user.model';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from '../../services/registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  public username: string = "";
  public email: string = "";
  public password: string = "";
  public confirmPassword: string = "";
  public user: LibraryUser;
  public name : string;
  public message: string;
  public registered: boolean;
  public captchaConfirmed = false;

  constructor(private service: RegistrationService, private router: Router) { }

  ngOnInit() {
    this.user = new LibraryUser();
    this.registered = true;
    this.message = "You've registered, check your e-mail to confirm your account!";
  }

  takeSignUpData(form: NgForm) {
    this.registered = true;
    this.message = "You've registered, check your e-mail to confirm your account!";
    this.user.dateJoined = new Date();
    this.newUser(form.value.name, form.value.username, form.value.password, form.value.email);
    this.service.register(this.user).subscribe(
      (data) => {
        this.user = data;
        this.message = "You've registered, check your e-mail to confirm your account!";
        this.registered = true;
      },
      (err) => {
        console.log(err);
        this.message = "Username or password already taken!";
        this.registered = false;
      }
    );
    console.log(this.user.username + " " + this.user.password);
  }

  newUser(name: string, uss: string, pass: string, mail: string) {
    this.user.username = uss;
    this.user.password = pass;
    this.user.email = mail;
    this.user.name = name;
  }


  onOk() {
    this.router.navigate(['login']);
  }

  resolved(captchaResponse: string) {
    this.service.captchaCheck(captchaResponse).subscribe(
      (data) => {
        if (data == true) {
          this.captchaConfirmed = true;
        }
      },
      (error) => {
        console.log(error);
      }
    )
  }

}
