import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LibraryUser } from '../../model/library-user.model';
import { LibraryUserService } from '../../services/library-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService, AuthUser } from '../../services/login.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css'],
  providers: [LibraryUserService]
})
export class LogInComponent implements OnInit {

  public username: string;
  public password: string;
  public email: string;
  public user: AuthUser;
  public message: string;
  public msg: string;
  public msgSent: boolean;
  public lgdin: boolean;
  private sub: any;
  private id: number;
  public captchaConfirmed = false;
  public commId: number;

  constructor(private loginService: LoginService, private userService: LibraryUserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    if (this.loginService.isUserAuth() == true) {
      location.reload();
    }

    this.user = new AuthUser();
    this.username = "";
    this.password = "";
    this.lgdin = false;
    this.getParamFromUrl();
  }

  public getParamFromUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.getCommIdUrl();
    });
  }
  
  public getCommIdUrl() {
    this.sub = this.route.params.subscribe(params => {
      this.commId = +params['commId'];
    });
  }

  takeSignInData(form: NgForm) {
    this.loginService.login(form.value.username, form.value.password).subscribe(
      (data) => {
        this.user = { ...data };
        this.lgdin = true;
        this.message = "Success!";
        form.reset();
      },
      (error) => {
        this.message = "Login failed!";
        this.lgdin = false;
      },
      () => {
        if (!this.user.emailConfirmed) {
          this.loginService.logout();
          this.lgdin = false;
          this.message = "You must confirm your account. Check your mail.";
        }
        if (this.user.blocked) {
          this.loginService.logout();
          this.lgdin = false;
          this.message = "Your account is blocked. You can't log in.";
        }
      }
    );
  }

  forgotPassword() {
    this.msg = "We've sent you an e-mail containing your new password!";
    this.msgSent = true;
    let user = new LibraryUser();
    user.email = this.email;
    this.userService.forgotPassword(user).subscribe(
      (data) => {
        this.msg = "We've sent you an e-mail containing your new password!";
        this.msgSent = true;
      },
      (error) => {
        this.msg = "We're sorry, this e-mail is not valid, you've probably made a mistake.";
        this.msgSent = false;
      }
    );
  }

  onOk() {
    if (this.id > 0) {
      this.router.navigate(['preview-book/' + this.id + "/" + this.commId]);
    } else {
      this.router.navigate(['home']);
    }
  }

  public resolved(captchaResponse: string) {
    this.loginService.captchaCheck(captchaResponse).subscribe(
      (data) => {
        if (data == true) {
          this.captchaConfirmed = true;
        }
      },
      (error) => {
        console.log(error);
      }
    )
  }

}
